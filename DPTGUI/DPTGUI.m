function varargout = DPTGUI(varargin)
    %   DPTGUI Double Pulse Test Graphical User Interface
    %
    %   Modified from Waveform Generator GUI 2
    %   (https://uk.mathworks.com/matlabcentral/fileexchange/35183-waveform-generator-gui-2)
    %
    %   Sam Walder - University of Bristol - 2016 - sam.walder@bristol.ac.uk
    %
    % =====================================================================
    %   Input arguments
    %       * 
    %   Output arguments
    %       * 
    %	Dependencies
    %		* interactive_curve.m - https://uk.mathworks.com/matlabcentral/fileexchange/35056-interactive-curve-class
    % =====================================================================
    %   Change Log:
    %       * 2016 - Modified from Waveform Generator GUI 2
    % =====================================================================

    % Begin initialization code - DO NOT EDIT... well, TBH, ignore that...
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @DPTGUI_OpeningFcn, ...
                       'gui_OutputFcn',  @DPTGUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
                   
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code


%=========================================================================%
% Opening Function
%=========================================================================%
function DPTGUI_OpeningFcn(hObject, eventdata, handles, varargin)
    %DPTGUI_OPENINGFCN Runs just before the figure becomes visible
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    %       * varargin  - Command line arguments to GUI
    % =====================================================================

    % Choose default command line output
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    % Initial states:
    mousemode = 1;  % move
    n = 10;         % number of markers
    XLim = [0 1];
    YLim = [0 1];
    st = 1/n; 
    markersX = XLim;
    markersY = YLim;

    % Set up the interactive curves
    ic(1) = interactive_curve(handles.figure1, handles.axes1, markersX, markersY, XLim, YLim);
    ic(2) = interactive_curve(handles.figure1, handles.axes2, markersX, fliplr(markersY), XLim, YLim);
    ic(3) = interactive_curve(handles.figure1, handles.axes3, markersX, markersY, XLim, YLim);
    ic(4) = interactive_curve(handles.figure1, handles.axes4, markersX, fliplr(markersY), XLim, YLim);

    % Place the ic handles into the handles structure
    handles.ic = ic;

    % Change some of the ic settings
    for i=1:4
        handles.ic(i).xMargin = 0;
        handles.ic(i).yMargin = 0;
        handles.ic(i).gap = st/8;
    end; % for

    % Update some of the UI elements with the values that have been setup
    set(handles.mode, 'value', mousemode);
    set(handles.editR1Period, 'string', num2str(1));
    
    % Set some default values into other bits
    set(handles.editOn1Period,  'string', num2str(1));
    set(handles.editOff1Period, 'string', num2str(1));
    set(handles.editOn2Period,  'string', num2str(1));
    set(handles.editOff2Period, 'string', num2str(1));
    set(handles.editOn1Amp,     'string', num2str(1));
    set(handles.editOff1Amp,    'string', num2str(0));
    set(handles.editOn2Amp,     'string', num2str(1));
    set(handles.editOff2Amp,    'string', num2str(0));
    
    set(handles.editR1Period,   'string', num2str(0.1));
    set(handles.editF1Period,   'string', num2str(0.1));
    set(handles.editR2Period,   'string', num2str(0.1));
    set(handles.editF2Period,   'string', num2str(0.1));
    
    
    % Run these objects callbacks so that the values we just put in
    % propergate
    editOn1Amp_Callback( handles.editOn1Amp,  eventdata, handles);
    editOff1Amp_Callback(handles.editOff1Amp, eventdata, handles);
    editOn2Amp_Callback( handles.editOn2Amp,  eventdata, handles);
    editOff2Amp_Callback(handles.editOff2Amp, eventdata, handles);
    
    editPeriod_Callback(handles.editR1Period, eventdata, handles);
    editPeriod_Callback(handles.editF1Period, eventdata, handles);
    editPeriod_Callback(handles.editR2Period, eventdata, handles);
    editPeriod_Callback(handles.editF2Period, eventdata, handles);

    % Update handles structure
    guidata(hObject, handles);

    
%=========================================================================%
% Output Function
%=========================================================================%
function varargout = DPTGUI_OutputFcn(hObject, eventdata, handles) 
    %DPTGUI_OUTPUTFCN Does something important... probably
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================

    % Get default command line output from handles structure
    varargout{1} = handles.output;
    
%=========================================================================%
% Delete Function
%=========================================================================%
function figure1_DeleteFcn(hObject, eventdata, handles)
    % Disconect the signal genorator (if it has been set up)
    if exist('handles.sigGen', 'var')
        SigGen_Stop(handles.sigGen);
    end; % if
    
    
%=========================================================================%
% getCurrentAxes Function
%=========================================================================%
function axesNo = getCurrentAxes(hObject)
    %GETCURRENTAXES Figures out which of the axes an object belongs to
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle of item to identify
    %   Output arguments
    %       * axesNo    - The number of the axes that the object belongs to
    % =====================================================================
    
    % This works by just knowing all of the tags in advanced. Simple
    switch hObject.Tag
        case 'editR1Period'
            axesNo = 1;
        case 'puR1InterpMethod'
            axesNo = 1;
        case 'puR1Preset'
            axesNo = 1;
        case 'axes1'
            axesNo = 1;
        case 'editF1Period'
            axesNo = 2;
        case 'puF1InterpMethod'
            axesNo = 2;
        case 'puF1Preset'
            axesNo = 2;
        case 'axes2'
            axesNo = 2;
        case 'editR2Period'
            axesNo = 3;
        case 'puR2InterpMethod'
            axesNo = 3;
        case 'puR2Preset'
            axesNo = 3;
        case 'axes3'
            axesNo = 3;
        case 'editF2Period'
            axesNo = 4;
        case 'puF2InterpMethod'
            axesNo = 4;
        case 'puF2Preset'
            axesNo = 4;
        case 'axes4'
            axesNo = 4;
    end
        

%=========================================================================%
% Period Callback
%=========================================================================%
function editPeriod_Callback(hObject, eventdata, handles)
    %EDITPERIOD_CALLBACK Runs when any period is changed
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % First identify which one of the graphs we are working with
    graphNumber = getCurrentAxes(hObject);
    
    % Get the old period    
    XLim = get(handles.ic(graphNumber).axesHandle, 'XLim');
    TOld = XLim(2) - XLim(1);
    
    % Get the new period and apply it to the axes
    T = str2double(get(hObject, 'String'));
    newXLim = [0 T];
    handles.ic(graphNumber).setXLim(newXLim);
  
    % Move the markers onto scaled positions
    handles.ic(graphNumber).setMarkersPositions(handles.ic(graphNumber).x*T/TOld, handles.ic(graphNumber).y);

    % Scale the gap and margin settings so that things still work nicely
    handles.ic(graphNumber).xMargin = 0; %(max(handles.ic(graphNumber).x)-min(handles.ic(graphNumber).x))/70;
    YLim = get(handles.ic(graphNumber).axesHandle, 'YLim');
    handles.ic(graphNumber).yMargin = 0; %0.05 * (YLim(2) - YLim(1));
    handles.ic(graphNumber).gap = T/70;
    handles.ic(graphNumber).xLine = linspace(newXLim(1), newXLim(2), 100);
    
    % Redraw the line
    handles.ic(graphNumber).redraw();
    
    % Tidy up the X axis
    % There are some issues with the use of this :(
    %tidyXAxis(handles.ic(graphNumber).axesHandle);


%=========================================================================%
% Interpolation Method Callback
%=========================================================================%
    function InterpMethod_Callback(hObject, eventdata, handles)
    %INTERPMETHOD_CALLBACK Runs when the interpolation methods are changed
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % First figure out which axes we are talkin about
    graphNumber = getCurrentAxes(hObject);
    
    % Get the interpolation method string from the dropdown box
    v = get(hObject,'Value');
    contents = cellstr(get(hObject,'String'));
    method = contents{v};
    
    % Give this to the ic
    handles.ic(graphNumber).setMethod(method);
    
    
%=========================================================================%
% Mouse mode callback function
%=========================================================================%
function mode_Callback(hObject, eventdata, handles)
    %MODE_CALLBACK Sets the mouse mode
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % Get the value from the dropdown box
    v = get(hObject,'Value');
    
    % Apply this to all the interactive curve objects
    for i=1:4
        handles.ic(i).mouseMode = v;
    end


%=========================================================================%
% 
%=========================================================================%
function mfile_Callback(hObject, eventdata, handles)
    %
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    [file,path] = uiputfile('*.m','generate m-file');
    fln=[path file];
    handles.ic.generateMFile(fln,true);
    edit(fln);
    
    
%=========================================================================%
% 
%=========================================================================%
function cb_copyToWorkspace(hObject, eventdata, handles)
    %
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    [XData, YData] = getWaveform(handles);
    DPTWaveform = [XData; YData];
    assignin('base', 'DPTWaveform', DPTWaveform);
  

%=========================================================================%
% On one amplitude callback
%=========================================================================%
function editOn1Amp_Callback(hObject, eventdata, handles)
    %EDITON1AMP_CALLBACK Runs when the amplitude of on1 is altered
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % Get the new value
    amplitude = str2double(get(hObject, 'String'));
    
    % Get the current Y limits
    a1YLim = get(handles.axes1, 'YLim');
    a2YLim = get(handles.axes2, 'YLim');
    
    % Set the Y Limits on A1 and A2 to go up to 'amplitude'
    set(handles.axes1, 'YLim', [a1YLim(1), amplitude]);
    set(handles.axes2, 'YLim', [a2YLim(1), amplitude]);

    
%=========================================================================%
% Off one amplitude callback
%=========================================================================%
function editOff1Amp_Callback(hObject, eventdata, handles)
    %EDITOFF1AMP_CALLBACK Runs when the amplitude of off1 is altered
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % Get the new value
    amplitude = str2double(get(hObject, 'String'));
    
    % Get the current Y limits
    a2YLim = get(handles.axes2, 'YLim');
    a3YLim = get(handles.axes3, 'YLim');
    
    % Set the Y Limits on A2 and A3 to go down to 'amplitude'
    set(handles.axes2, 'YLim', [amplitude, a2YLim(2)]);
    set(handles.axes3, 'YLim', [amplitude, a3YLim(2)]);
    
    
%=========================================================================%
% On two amplitude callback
%=========================================================================%
function editOn2Amp_Callback(hObject, eventdata, handles)
    %EDITON2AMP_CALLBACK Runs when the amplitude of on2 is altered
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % Get the new value
    amplitude = str2double(get(hObject, 'String'));
    
    % Get the current Y limits
    a3YLim = get(handles.axes3, 'YLim');
    a4YLim = get(handles.axes4, 'YLim');
    
    % Set the Y Limits on A3 and A4 to go up to 'amplitude'
    set(handles.axes3, 'YLim', [a3YLim(1), amplitude]);
    set(handles.axes4, 'YLim', [a4YLim(1), amplitude]);
    
    
%=========================================================================%
% Off two amplitude callback
%=========================================================================%
function editOff2Amp_Callback(hObject, eventdata, handles)
    %EDITOFF2AMP_CALLBACK Runs when the amplitude of off2 is altered
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % Get the new value
    amplitude = str2double(get(hObject, 'String'));
    
    % Get the current Y limits
    a4YLim = get(handles.axes4, 'YLim');
    a1YLim = get(handles.axes1, 'YLim');
    
    % Set the Y Limits on A4 and A1 to go down to 'amplitude'
    set(handles.axes4, 'YLim', [amplitude, a4YLim(2)]);
    set(handles.axes1, 'YLim', [amplitude, a1YLim(2)]);


%=========================================================================%
% Redraw Preview Fucntion
%=========================================================================%
function redrawPreview_Callback(hObject, eventdata, handles)
    %REDRAWPREVIEW_CALLBACK Redraws the preview plot
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle to figure
    %       * eventdata - Reserved
    %       * handles   - Structure with handles and user data
    % =====================================================================
    
    % First get the full waveform
    [XData, YData] = getWaveform(handles);
    
    % Now plot it
    plot(handles.axes5, XData, YData);

    
%=========================================================================%
% Get Waveform Function
%=========================================================================%
function [XData, YData] = getWaveform(handles)
    %GETWAVEFORM Gets the full DPT waveform from the GUI
    %
    % =====================================================================
    %   Input arguments
    %       * handles - The handles in the current figure
    %   Output arguments
    %       * XData - X data of the waveform
    %       * YData - Y data of the waveform
    % =====================================================================
    
    % Fist get all of the edges from the ic objects
    XEdgeVectors = cell(1, 4);
    YEdgeVectors = cell(1, 4);
    for i=1:4
        XEdgeVectors{i} = handles.ic(i).lineHandle.XData;
        YEdgeVectors{i} = handles.ic(i).lineHandle.YData;
    end
    
    % Get the period of each of the gaps
    XFlatPeriods(1) = str2double(handles.editOn1Period.String);
    XFlatPeriods(2) = str2double(handles.editOff1Period.String);
    XFlatPeriods(3) = str2double(handles.editOn2Period.String);
    XFlatPeriods(4) = str2double(handles.editOff2Period.String);

    % Apply the offsets to the X vectors
    for i=2:4
        XEdgeVectors{i} = XEdgeVectors{i} + max(XEdgeVectors{i-1}) + XFlatPeriods(i-1);
    end
    
    % Now we need to string all of this together remembering the last point
    XData = [cell2mat(XEdgeVectors), max(XEdgeVectors{end})+XFlatPeriods(end)];
    YData = [cell2mat(YEdgeVectors), YEdgeVectors{1,4}(end)];
    
    
%=========================================================================%
% Connnect Signal Genorator Callback
%=========================================================================%
function sigGenConnect(hObject, eventdata, handles)
    % Conect the signal genorator
    handles.sigGen = SigGen_Start;
    
    % Update handles structure
    guidata(hObject, handles);

    
%=========================================================================%
% Disconnnect Signal Genorator Callback
%=========================================================================%
function sigGenDisconnect(hObject, eventdata, handles)
    SigGen_Stop(handles.sigGen);
  
    
%=========================================================================%
% Upload Data Callback
%=========================================================================%   
function upload_Callback(hObject, eventdata, handles)
    % Get the waveform data
    [XData, YData] = getWaveform(handles);
    
    % Upload this data
    SigGen_LoadWaveform_33220A(handles.sigGen, XData, YData);
    
%=========================================================================%
% Upload + Trig Data Callback
%=========================================================================%   
function uploadAndTrig_callback(hObject, eventdata, handles)
    % This will upload the waveform and then run the trigger function
    % short time later to reduce mouse clicks and mean faster testing
    
     % Get the waveform data
    [XData, YData] = getWaveform(handles);
    
    % Upload this data, with the safety check disabled
    SigGen_LoadWaveform_33220A(handles.sigGen, XData, YData, 666);
    
    % Wait a moment
    pause(0.5);
    
    % Trigger!
    SigGen_Trig(handles.sigGen);
    
%=========================================================================%
% Safety Checkbox Callback
%=========================================================================%   
function safetySwitch_callback(hObject, eventdata, handles)
    if hObject.Value
        % If the box is checked mak the button active
        handles.uploadandtrig.Enable = 'on';
    else
        % If not make the button inactive
        handles.uploadandtrig.Enable = 'off';
    end; % if
    
%=========================================================================%
% Trigger Button Callback
%=========================================================================%   
function trigger_Callback(hObject, eventdata, handles)
    % Trigger the sigGen output
    SigGen_Trig(handles.sigGen);
    
%=========================================================================%
% Save/Recall: Set current directory button
%=========================================================================%
function pushbutton11_Callback(hObject, eventdata, handles)
        set(handles.edit20, 'String', pwd);
        updateFilename(handles);
        updateFilelist(handles);

%=========================================================================%
% Save/Recall: Prefix text
%=========================================================================%
function edit21_Callback(hObject, eventdata, handles)
    % Update the next name
    updateFilename(handles); 

%=========================================================================%
% Save/Recall: Save button
%=========================================================================%
function pushbutton12_Callback(hObject, eventdata, handles)
    % Get the working directory
    workingDirectory = get(handles.edit20, 'String');

    % Get the prefix
    prefix = get(handles.edit21, 'String');

    % Figure out which file number we have gotten to
    numberFiles = nFiles(workingDirectory, prefix);

    % Get the file name
    filename = ['DPT_', prefix, '_', sprintf('%03.0f', numberFiles+1)];
    
    % Run the figure data saving tool
    DPTGUISaveData = DPTGetSettings(handles);
    
    % Save the data
    save([workingDirectory, '\', filename], 'DPTGUISaveData');
    assignin('base', filename, DPTGUISaveData);

    % Run update filename function
    updateFilename(handles);

    % Update the list of files in the popup
    updateFilelist(handles);

%=========================================================================%
% Save/Recall: Recall Button
%=========================================================================%
function pushbutton13_Callback(hObject, eventdata, handles)
    % Get the working directory
    workingDirectory = get(handles.edit20, 'String');

    % Get the right filename
    selectedItem = get(handles.popupmenu11, 'Value');
    filenames = get(handles.popupmenu11, 'String');
    filename = filenames{selectedItem};

    % Load in the right file
    importedData = load(strcat(workingDirectory, '\DPT_', filename, '.mat'));
    
    % Run the recall tool
    DPTSetSettings(handles, importedData.DPTGUISaveData);
    
    % Redraw the preview
    % First get the full waveform
    [XData, YData] = getWaveform(handles);
    % Now plot it
    plot(handles.axes5, XData, YData);
    
    % Update handles structure
    guidata(hObject, handles);
    
    
%=========================================================================%
% Trigger Inf. Button
%=========================================================================%
function pushbutton14_Callback(hObject, eventdata, handles)
    % Construct a questdlg with three options
    choice = questdlg('Pressing OK will start the signal genorator output in continuouse mode. Are you sure you want to do this?', ...
        'Warning!', ...
        'Yes', 'No', 'No');
    % Handle response
    switch choice
        case 'Yes'
            % Just allow it to carry on
        otherwise
            % Stop it all
            error('User terminated operation');
    end
    
    % Turn the output off
    fprintf(handles.sigGen, 'OUTPut OFF');
    
    % Turn off burst mode
    if strcmp('33120A', SigGen_Model(handles.sigGen))
        fprintf(handles.sigGen,'BM:STATE OFF');
    elseif strcmp('33220A', SigGen_Model(handles.sigGen))
        fprintf(handles.sigGen,'BURSt:STATe OFF');
    elseif strcmp('33522B', SigGen_Model(handles.sigGen))
        fprintf(handles.sigGen,'BURSt:STATe OFF');
    else
        error('Failed to turn off burst mode');
    end; % if
    
    % Turn the output on
    fprintf(handles.sigGen, 'OUTPut ON');
    
%=========================================================================%
% Stop Button
%=========================================================================%
function pushbutton15_Callback(hObject, eventdata, handles)
    % Turn the output off
    fprintf(handles.sigGen, 'OUTPut OFF');
    
%=========================================================================%
%=========================================================================%
% Custom Functions
%=========================================================================%
%=========================================================================%

    %=========================================================================%
    % Number of files
    %=========================================================================%
    function count = nFiles(cwd, prefix)
        % Gets the number of DPT files that are already saved in the
        % current directory with the given prefix
        % =====================================================================
        %   Input arguments
        %       * cwd    - The present working directory
        %       * prefix - The prefix to check for
        % =====================================================================

        % Fist get all DPT files in the current directory
        DPTfile = listDPTFiles(cwd);

        % Filter these to just those with the given prefix
        matchedFiles = cell(length(DPTfile), 1);
        x = 1;
        for i=1:length(DPTfile)
            stamp = DPTfile{i}(1:end-4);
            if strcmp(prefix, stamp)
                % We have a hit!
                matchedFiles{x} = DPTfile{i}(end-2:end);
                x = x+1;
            end; % if
        end; % for
        matchedFiles = matchedFiles(1:x-1);

        % Find the largest number associated with one of these
        max = 0;
        for i=1:length(matchedFiles)
            number = str2double(matchedFiles{i});
            if number > max
                max = number;
            end; % If
        end; % For

        count = max;


    %=========================================================================%
    % List of files
    %=========================================================================%
    function list = listDPTFiles(cwd)
        % Gets a list of all the DPT files in a directory
        % =====================================================================
        %   Input arguments
        %       * cwd    - The present working directory
        % =====================================================================

        % Fist get all the objects in the current directory
        allObjects = dir(cwd);

        % Now, figure out which of these are DPT saves
        DPTFiles = cell(length(allObjects), 1);
        x = 1;
        for i=1:length(allObjects)
            if length(allObjects(i).name)>6
                beggining = allObjects(i).name(1:4);
                if strcmp('DPT_', beggining)
                    % We have a hit!
                    DPTFiles{x} = allObjects(i).name(5:end-4);
                    x = x+1;
                end; % if
            end; % if
        end; % for
        list = DPTFiles(1:x-1);

    %=========================================================================%
    % Update next file name function
    %=========================================================================%
    function updateFilename(handles)
        % UPDATEFILENAME updates the displayed next file name

        % Get the working directory
        workingDirectory = get(handles.edit20, 'String');

        % Get the prefix
        prefix = get(handles.edit21, 'String');

        % Get the number of files
        numberFiles = nFiles(workingDirectory, prefix);

        % Set the next filename
        nextFilename = ['DPT_', prefix, '_', sprintf('%03.0f', numberFiles+1)];
        set(handles.text43, 'String', ['Next Name: ', nextFilename]);


    %=========================================================================%
    % Update files list function
    %=========================================================================%
    function updateFilelist(handles)
        % UPDATEFILELIST updates the list of files that can be recalled

        % Get the working directory
        workingDirectory = get(handles.edit20, 'String');

        % Get the list of files
        list = listDPTFiles(workingDirectory);

        % Check for an empty list
        if isempty(list)
            list = '-No files found-';
        end; % if

        % Put this in the listbox (Set value to 1 first to avoide problems)
        set(handles.popupmenu11, 'Value', 1)
        set(handles.popupmenu11, 'String', list)
        
    %=========================================================================%
    % DPT GUI setting saving tool
    %=========================================================================%
    function DPTSaveData = DPTGetSettings(handles)
        % DPTGETSETTINGS gets all of the settings from the DPTGUI
        
        for transitionNo = 1:4
            % Interactive curve settings
            DPTSaveData.transition(transitionNo).intCurve.interpMethod = handles.ic(transitionNo).method;
            DPTSaveData.transition(transitionNo).intCurve.xMarks = handles.ic(transitionNo).x;
            DPTSaveData.transition(transitionNo).intCurve.yMarks = handles.ic(transitionNo).y;
            
            DPTSaveData.transition(transitionNo).intCurve.xMargin = handles.ic(transitionNo).xMargin;
            DPTSaveData.transition(transitionNo).intCurve.yMargin = handles.ic(transitionNo).yMargin;
            DPTSaveData.transition(transitionNo).intCurve.gap = handles.ic(transitionNo).gap;
        end; %for
        
        % Unfortunately we did not do the creation of all the boxes in a
        % programatically friendy way, so each thing has to be accessed
        % directly in a list :(
        
            % T1 stuff
            DPTSaveData.transition(1).transitionPeriod = str2double(get(handles.editR1Period, 'String'));
            DPTSaveData.transition(1).flatPeriod =  str2double(get(handles.editOn1Period, 'String'));
            DPTSaveData.transition(1).flatAmp =  str2double(get(handles.editOn1Amp, 'String'));
            DPTSaveData.transition(1).interpValue = get(handles.puR1InterpMethod, 'Value');
            DPTSaveData.transition(1).XLim = get(handles.axes1, 'XLim');
            DPTSaveData.transition(1).YLim = get(handles.axes1, 'YLim');
        
            % T2 stuff
            DPTSaveData.transition(2).transitionPeriod = str2double(get(handles.editF1Period, 'String'));
            DPTSaveData.transition(2).flatPeriod =  str2double(get(handles.editOff1Period, 'String'));
            DPTSaveData.transition(2).flatAmp =  str2double(get(handles.editOff1Amp, 'String'));
            DPTSaveData.transition(2).interpValue = get(handles.puF1InterpMethod, 'Value');
            DPTSaveData.transition(2).XLim = get(handles.axes2, 'XLim');
            DPTSaveData.transition(2).YLim = get(handles.axes2, 'YLim');
            
            % T3 stuff
            DPTSaveData.transition(3).transitionPeriod = str2double(get(handles.editR2Period, 'String'));
            DPTSaveData.transition(3).flatPeriod =  str2double(get(handles.editOn2Period, 'String'));
            DPTSaveData.transition(3).flatAmp =  str2double(get(handles.editOn2Amp, 'String'));
            DPTSaveData.transition(3).interpValue = get(handles.puR2InterpMethod, 'Value');
            DPTSaveData.transition(3).XLim = get(handles.axes3, 'XLim');
            DPTSaveData.transition(3).YLim = get(handles.axes3, 'YLim');
            
            % T4 stuff
            DPTSaveData.transition(4).transitionPeriod = str2double(get(handles.editF2Period, 'String'));
            DPTSaveData.transition(4).flatPeriod =  str2double(get(handles.editOff2Period, 'String'));
            DPTSaveData.transition(4).flatAmp =  str2double(get(handles.editOff2Amp, 'String'));
            DPTSaveData.transition(4).interpValue = get(handles.puF2InterpMethod, 'Value');
            DPTSaveData.transition(4).XLim = get(handles.axes4, 'XLim');
            DPTSaveData.transition(4).YLim = get(handles.axes4, 'YLim');
        
%=========================================================================%
% DPT GUI setting recall tool
%=========================================================================%
function DPTSetSettings(handles, DPTSaveData)
    % DPTGETSETTINGS sets all of the settings from the DPTGUI
    
    % Unfortunately we did not do the creation of all the boxes in a
    % programatically friendy way, so each thing has to be accessed
    % directly in a list :(

        % If the Recall on/off periods checkbox is true then we get those
        if handles.recallMapOnOff.Value
            for transitionNo = 1:4
                set(transitionHandle(handles, 'FlatPeriod', transitionNo), 'String', DPTSaveData.transition(transitionNo).flatPeriod);
                set(transitionHandle(handles, 'FlatAmp', transitionNo), 'String', DPTSaveData.transition(transitionNo).flatAmp);
            end; % for
        end; % if
        
        % Get the mapping of each edge
        edge(1).source = handles.recallMapR1.Value;
        edge(2).source = handles.recallMapF1.Value;
        edge(3).source = handles.recallMapR2.Value;
        edge(4).source = handles.recallMapF2.Value;
        
        % Apply the settings to each edge
        for transitionNo = 1:4
            % If the edge is to be recalled
            if edge(transitionNo).source ~= 5
                % GUI Settings
                set(transitionHandle(handles, 'Period', transitionNo), 'String', DPTSaveData.transition(edge(transitionNo).source).transitionPeriod);
                set(transitionHandle(handles, 'Interp', transitionNo), 'Value', DPTSaveData.transition(edge(transitionNo).source).interpValue);

                % Interactive curve settings
                handles.ic(transitionNo).setMarkersPositions(DPTSaveData.transition(edge(transitionNo).source).intCurve.xMarks, DPTSaveData.transition(edge(transitionNo).source).intCurve.yMarks);
                handles.ic(transitionNo).setMethod(DPTSaveData.transition(edge(transitionNo).source).intCurve.interpMethod);

                handles.ic(transitionNo).setXLim(DPTSaveData.transition(edge(transitionNo).source).XLim);
                handles.ic(transitionNo).setYLim(DPTSaveData.transition(edge(transitionNo).source).YLim);

                handles.ic(transitionNo).xMargin = DPTSaveData.transition(edge(transitionNo).source).intCurve.xMargin;
                handles.ic(transitionNo).yMargin = DPTSaveData.transition(edge(transitionNo).source).intCurve.yMargin;
                handles.ic(transitionNo).gap = DPTSaveData.transition(edge(transitionNo).source).intCurve.gap;

                % Reset the XLine stuff (I dunno, see the classdef)
                handles.ic(transitionNo).xLine = linspace(DPTSaveData.transition(edge(transitionNo).source).XLim(1), DPTSaveData.transition(edge(transitionNo).source).XLim(2), 100);

                % Redraw the line
                handles.ic(transitionNo).redraw();
            end; % if
            
        end; %for
    
%=========================================================================%
% DPT GUI setting recall tool
%=========================================================================%
function handle = transitionHandle(handles, type, transitionNumber)
    % TRANSITIONHANDLE Gets the handle of the object typle for the given
    % transition
    % =====================================================================
    %   Input arguments
    %       * handles          - The handles structure for this figure
    %       * type             - The type you are looking for:
    %                           * Period
    %                           * Interp
    %                           * FlatPeriod
    %                           * FlatAmp
    %       * transitionNumber - The transition to look in (1:4)
    %   Output arguments
    %       * handle           - A handle to the desired object
    % =====================================================================
    
    switch type
        case 'Period'
            switch transitionNumber
                case 1
                    handle = handles.editR1Period;
                case 2
                    handle = handles.editF1Period;
                case 3
                    handle = handles.editR2Period;
                case 4
                    handle = handles.editF2Period;
                otherwise
                    error('Unknown transition');
            end; % Switch
            
            case 'Interp'
                switch transitionNumber
                    case 1
                        handle = handles.puR1InterpMethod;
                    case 2
                        handle = handles.puF1InterpMethod;
                    case 3
                        handle = handles.puR2InterpMethod;
                    case 4
                        handle = handles.puF2InterpMethod;
                    otherwise
                        error('Unknown transition');
                end; % Switch
            
            case 'FlatPeriod'
                switch transitionNumber
                    case 1
                        handle = handles.editOn1Period;
                    case 2
                        handle = handles.editOff1Period;
                    case 3
                        handle = handles.editOn2Period;
                    case 4
                        handle = handles.editOff2Period;
                    otherwise
                        error('Unknown transition');
                end; % Switch
                
            case 'FlatAmp'
                switch transitionNumber
                    case 1
                        handle = handles.editOn1Amp;
                    case 2
                        handle = handles.editOff1Amp;
                    case 3
                        handle = handles.editOn2Amp;
                    case 4
                        handle = handles.editOff2Amp;
                    otherwise
                        error('Unknown transition');
                end; % Switch
                
        otherwise
            error('Unknown type');
    end; % Switch
    
    
    % Update handles structure
    %guidata(hObject, handles);
