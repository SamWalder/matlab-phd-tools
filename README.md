# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* This is a set of tools that I have been developing through the course of my studies. It is now available here so that I can share the development of new features with everyone.
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

### Summary of set up ###

Pull this repository to your machine and most of the tools should work. Instructions for particular tools should be contained in their folders.

### Configuration ###

*Currently this project is being developed in Matlab R2016a

### Dependencies ###

You will also need the following for all of the tools to work correctly:
* export_fig: https://github.com/ojwoodford/export_fig

## Contribution guidelines ##

Please feel free to contribute any improvements, though please make sure that changes retain compatibility  and that code is well commented.

## Who do I talk to? ##

* Sam Walder - sam.walder@bristol.ac.uk