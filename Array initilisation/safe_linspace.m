function [ array ] = safe_linspace(x1, x2, varargin)
%   SAFE_LINSPACE - Creates an linear array of number the same as linspace
%
%   Sam Walder - University of Bristol - 2015 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments:
%       * x1 - 
%       * x2 - 
%       * n  - *Optional* - 
%   Output arguments
%       * array - The array of numbers
% =========================================================================
%   Change Log:
%       * 2015 - Created
% =========================================================================
 
safety_factor = 0.1; % Memory to reserve, just to be safe
 
%% Check number of inputs and act accordingly

if nargin == 2
    % User has not defined n 
    n = 100;
elseif nargin == 3
    % User has defined n
    n = varargin{1};
else
    % Cause an error
    error('Invalid number of arguments');
end; % if


%% Evaluate demaded size of the array
sizeof_double = 8; % Bytes
demanded_memory = sizeof_double * n;

% Introduce safety factor
demanded_memory = demanded_memory * (1+safety_factor);

%% Get avalible memory
[uV, sV] = memory;
Physicalmemory = sV.PhysicalMemory.Available;

%% Create array or throw error
if demanded_memory < Physicalmemory
    % Create the array
    array = linspace(x1, x2, n);
    %disp(strcat('Demand = ', num2eng(demanded_memory)));
    %disp(strcat('Avalible = ', num2eng(Physicalmemory)));
else
    % Tell the user no
    error('Not enough memory to create array');
end; % if


end % function