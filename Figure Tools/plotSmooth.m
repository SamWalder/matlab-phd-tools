function plotSmooth(handle)
%PLOTSMOOTH smooths out the lines on a figure for legibility
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * handle - Handel to axis, figure or line to work on
%   Output arguments
%       * 
%	Dependencies
%		*
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

%% Dafaults for unspecified variables
if nargin < 1
    % We do not have a handle to work with, so use the current figure
    handle = gcf;
else
    % They have specified the handle to work with
end % if

%% Get the handles of all the lines
% Behavure varies depending on type of handle
if strcmp(get(handle, 'type'), 'line')
    % We have been given just one line to work on
    lines{1} = handle;
elseif strcmp(get(handle, 'type'), 'axes')
    % We have been given one set of axes to get the lines from
    children = get(handle, 'children');
    
    lines = cell(1, length(children));
    n = 1;
    
    for i = 1:length(children)
        if strcmp(get(children(i), 'type'), 'line')
            lines{n} = children(i);
            n = n + 1;
        end; % if
    end; % for
    lines = lines(1:n-1);
    
elseif strcmp(get(handle, 'type'), 'figure')
    % We have been given a whole figure, which is a bit more complex to
    % work with.
    % Get all the axes first
    children = get(handle, 'children');
    
    axis = cell(1, length(children));
    n = 1;
    
    for i = 1:length(children)
        if strcmp(get(children(i), 'type'), 'axes')
            axis{n} = children(i);
            n = n + 1;
        end; % if
    end; % for
    axis = axis(1:n-1);
    
    % Now get all the lines
    lines = {};
    for ii = 1:length(axis)
        children = get(axis{ii}, 'children');
    
        n = length(lines) + 1;
        lines = [lines, cell(1, length(children))];

        for i = 1:length(children)
            if strcmp(get(children(i), 'type'), 'line')
                lines{n} = children(i);
                n = n + 1;
            end; % if
        end; % for
        lines = lines(1:n-1);
    end; % for
    
end; % if


%% Smooth each of the lines
for i = 1:length(lines)
    set(lines{i}, 'YData', smooth(get(lines{i}, 'YData')));
end; % for


