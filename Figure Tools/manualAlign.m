function offsets = manualAlign()
%MANUALALIGN Sets the position in a figure to align to
%   This function sets the reference point that will be used by
%   "manualAlign_moveLine" to line waveforms up to.
%   
%   Sam Walder - University of Bristol - 2013
%
% =========================================================================
%   Input arguments:
%       * 
%   Output arguments
%       * offsets - The amount by which each waveform was moved
%
% =========================================================================

%% Get the current figure engaged
dcm_obj = datacursormode(gcf);
set(dcm_obj,'DisplayStyle','datatip',...
    'SnapToDataVertex','off','Enable','on')

%% Ask the user to set the reference position
disp('Create a datatip at the point you wish to align things to, then press Return.')
% Wait while the user does this.
pause 

c_info = getCursorInfo(dcm_obj);
ref_position = c_info.Position(1);

% Display the refence position to the user
disp(strcat('Result = ', num2eng(ref_position)));

%% Draw a reference line
% Fill this in later

%% Ask the user to select the line(s) to be moved and move it
n = input('How many lines do you intend to move?');
offsets = zeros(n, 1);
for i=1:n
    disp('Click on the line you would like to be moved at the point you would like to align to the reference, then press Return.')
    % Wait while the user does this.
    pause 

    c2_info = getCursorInfo(dcm_obj);

    % Figure out what time offset we need to apply
    offset = c2_info.Position(1) - ref_position;
    offsets(i) = offset;

    % Apply this offset
    set(c2_info.Target, 'xdata', get(c2_info.Target, 'xdata') - offset);
    disp('Sorted mate!');
end; % for

end

