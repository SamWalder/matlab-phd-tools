function drawLine()
%Converts a drawn line object to a plotted line
%
%   To use, first draw a line object on your figure. Then make sure that
%   the line is selected and run this function. This will then replace that
%   line with a plotted one in the same position.
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%	Dependencies
%		*
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

% Organise the handles to the objects we will be working with
axeHand = gca;
linHand = gco;

% Set the units to normalized
set(axeHand, 'Units', 'Normalized');
set(linHand, 'Units', 'Normalized');

% Note down the position bits
axePos = axeHand.Position;
linPos = linHand.Position;

axeXOff = axePos(1);
axeXLen = axePos(3);
linXOff = linPos(1);
linXLen = linPos(3);

axeYOff = axePos(2);
axeYLen = axePos(4);
linYOff = linPos(2);
linYLen = linPos(4);

% Figure out the units per Normalized unit
XRange = axeHand.XLim(2) - axeHand.XLim(1);
xPerNorm = XRange / axeXLen;

YRange = axeHand.YLim(2) - axeHand.YLim(1);
yPerNorm = YRange / axeYLen;

% Calculate the coordinates of the line start
linXStart = (linXOff-axeXOff) * xPerNorm + axeHand.XLim(1);
linYStart = (linYOff-axeYOff) * yPerNorm + axeHand.YLim(1);

% Calculate the coordinates of the line end
linXEnd = linXStart + linXLen * xPerNorm;
linYEnd = linYStart + linYLen * yPerNorm;

% Plot a line in place of this thing
hold on;
plot([linXStart, linXEnd], [linYStart, linYEnd],...
    'Color', linHand.Color,...
    'LineStyle', linHand.LineStyle,...
    'LineWidth', linHand.LineWidth);

% Delete the ordiginal line
delete(linHand);
