function saveFigure(figureHandle,location,name)
%SAVEFIGURE Saves a figure to a .PNG and a .MAT
%   This function saves the given figure to the given location with the
%   given name as a 300 dpi png and also as a .mat for convinience. It
%   automaticvally applies the export setting for the png so that you don't
%   have to do it every single time you want to save a silly little figure.
%
%   Sam Walder - University of Bristol - 2013
%
% =========================================================================
%   Input arguments:
%       * figureHandle - Handle to the figure you want to print (use gcf)
%       * location - the file location you want to save it to as a string
%       (probabbly want to use pwd)
%       * name - the name that you want to give to the files (such as
%       'name')
%   Output arguments
%       * ~
%
% =========================================================================

fullSavePath = strcat(location,'/',name);

% Check if the file already exists
if exist(strcat(fullSavePath,'.fig'), 'file') == 2
    warningMessage = sprintf('Warning: file already exists:\n%s', name);
    error(warningMessage);
end; % if

% Set the background color to white so that it does not look aweful when
% saved
set(figureHandle, 'color', 'white');

% Save the .FIG
hgsave(figureHandle, strcat(fullSavePath,'.fig'), '-v7.3');

% Save the PNG
%print(figureHandle,'-dpng','-r300',strcat(fullSavePath,'.png'));
% Updated and added the following line after finding that this function is
% MUCH more powerful!
export_fig(strcat(fullSavePath,'.png'), '-png', '-r1000');
export_fig(strcat(fullSavePath,'.pdf'), '-pdf', '-q101', '-p0.01'); % Added the -p switch to add a bit of padding as some borders were being cut off

end

