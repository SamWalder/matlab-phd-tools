function y_axis_to_dBx( x )
%Y_AXIS_TO_DBX converts a plots y_axis to a dB scale relative to x
%   Takes the currently selected plot and reads in all the y_data. It then
%   takes 20log10(y_data/x) and replots it. The function will also make
%   ammendamants to the y_ticks and Y-lims to make sure the plot is still
%   usefull
%
%   Sam Walder - University of Bristol - 2015
%
% =========================================================================
%   Input arguments:
%       * x - the relative value to which the dB ratio is taken
%   Output Arguments
%       * none
%   Dependacies
%       * tidyYAxis()
%
% =========================================================================

% Get the current axis and its children
children = get(gca, 'children');
no_children = length(children);
% Check all the children are lines and remove if they are not
lines = children;
skipped = 0;
for i=1:no_children
    if strcmp(get(children(i), 'type'), 'line')
        lines(i-skipped) = children(i);
    else
        skipped = skipped+1;
    end; % if
end; % for

if skipped >= no_children
    error('None of the children found are lines');
end; % if

no_lines = no_children-skipped;

% Get the y_data from each of the children
y_data = cell(no_lines, 1);
for i=1:length(children)
    y_data{i} = get(children(i), 'YData');
end; % for

% Convert the information to relative dB
for i=1:no_lines
    for n=1:length(y_data{i, 1})
        y_data{i, 1}(n) = 20*log10(y_data{i, 1}(n)/x);
    end; % for
end; % for

% Change the y_axis scale to linear
set(gca, 'YScale', 'linear');

% Update the y_data for each line
n = 1;
for i=1:no_children
    if strcmp(get(children(i), 'type'), 'line')
        set(children(i), 'YData', y_data{n});
        n = n+1;
    end; % if
end; % for
        
% Update the y_axis Label
ylabel = get(get(gca, 'Ylabel'), 'string');
ylabel = strcat(ylabel, ' (dB_', num2str(x), ')');
set(get(gca, 'Ylabel'), 'string', ylabel);

% Update the y_axis limits
ylim = get(gca, 'YLim');
ylim(1) = 20*log10(ylim(1)/x);
ylim(2) = 20*log10(ylim(2)/x);
set(gca, 'YLim', ylim);

% Updatae the y_axis tick marks
set(gca, 'YTickLabelMode', 'auto');
set(gca, 'YTickMode', 'auto');

end

