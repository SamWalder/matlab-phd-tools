function plotRecolor( varargin )
%   Recolors the lines on the given handle
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * handle - The handle to a figure or set of axes (optional)
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

%% Figure out what input has been given
if nargin == 0
    % Nothing given
    inHandle = gcf;
elseif nargin == 1
    % Handle has been given
    inHandle = varargin{1};
else
    error('Wrong number of input aruments');
end; % if
    
%% Get the lines
% Find the type
lines = findobj(inHandle, 'Type', 'line');

%% Recolor the lines
for iCount = 1:length(lines)
    set(lines(iCount), 'color', cmrmap(length(lines), iCount));
end

