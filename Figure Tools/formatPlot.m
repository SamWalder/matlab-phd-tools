function formatPlot(varargin)
% FormatPlot - Formats the axis and fonts of a plot to make it look a bit
% nicer
%
%   Sam Walder - University of Bristol - 2014
%
% =========================================================================
%   Input arguments:
%       * varargin - Optional argument that can either be a figure handle
%       or and axis handle for the function to run on
%   Output arguments
%       * 
%
% =========================================================================
%   Change Log:
%       * 2014 - Created
%       * 2016 - Sam Walder
%           * Added a single input argument to act on a given handle
% =========================================================================

%% USER INPUT SETTINGS %
font_size = 22;
font_name = 'Times New Roman';
lineWidth = 2;

%% Check whether we were given an input or not
if nargin == 0
    % Nothing interesting happened, work on the current figure
    figure = gcf;
    children = get(figure, 'Children');
elseif nargin == 1
    % We now need to see if we were passed a figure or an axis handel
    if strcmp(get(varargin{1}, 'type'), 'figure')
        % we have been given a figure handle
        figure = varargin{1};
        children = get(figure, 'Children');
    elseif strcmp(get(varargin{1}, 'type'), 'axes')
        children = varargin{1};
    end;
else
    % Wrong number of arguments
    error('Invalid number of input arguments');
end;

%% Do the formatting
axis = cell(1,1);
n = 1;
for i=1:length(children)
    if (strcmp(get(children(i), 'type'), 'axes'))
        axis{n,1} = children(i);                        % Get the handle
        n = n+1;
    end % if
end % for loop

numberOfAxes = n-1;

% Do the following for every axis
for i=1:numberOfAxes
    % Turn the box off
    set(axis{i},'box','off');

    % Set the tick direction to out
    set(axis{i},'tickdir','out');

    % Set the axis fontsize
    set(axis{i},'fontsize',font_size);

    % Set font
    set(axis{i},'fontname',font_name);

    % Set axis linewidth
    set(axis{i}, 'linewidth', 1);
    
    % Set Y axis stuff
    y_label = get(axis{i},'ylabel');
    set(y_label,'fontsize',font_size);
    set(y_label,'fontname',font_name);
    set(y_label,'fontweight','light');
    
    % For X axis stuff we setup the first one as normal and make the rest
    % blank to save them from interfearing if overlayed
    x_label = get(axis{i},'xlabel');
    if i == 1
        set(x_label,'fontsize',font_size);
        set(x_label,'fontname',font_name);    
        set(x_label,'fontweight','light');
    else
        set(x_label,'String','');
    end % if
    
    % Find each of the lines on the axes
    lines = findobj(axis{i}, 'Type', 'line');
    
    for ii = 1:length(lines)
        set(lines(ii), 'LineWidth', 2);
    end; % for
    
end % for

% Set the legend font size independantly
set(legend, 'FontSize', 14);

% Set legend background color
set(legend, 'color', 'none');



end % function


    