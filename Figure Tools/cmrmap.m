function [aCMRmap] = cmrmap(n, m)
%CMRMAP Creates an n-point colourmap which translates to unique greyscale intensities
%   http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=01028735
%
%   Niall Oswald - University of Bristol
%   Modified - Sam Walder - University of Brsitol - 2014
%       -> Update header and comments
%       -> Changed Name
%       -> Allo selection of a colour m from n
%
% =========================================================================
%   Input arguments:
%       * n  - Number of colours you would like in the colour map
%       * m  - Use this to return just colur m from the n genorated colours
%   Output arguments
%       * aCMRmap - The colour map in RGB format (Returns just a single
%       colour if 'm' is used)
%
% =========================================================================

    % Define CMRmap
    CMRmap = [
    0.90 0.75 0.10;
    0.90 0.50 0.00;
    1.00 0.25 0.15;
    0.60 0.20 0.50;
    0.30 0.15 0.75;
    0.15 0.15 0.50;];
    %0.90 0.90 0.50;];  % Exclude this colour if it is too close to black
    %for your licking

    nIn = 6;
    nInt = ((nIn-1)*(n-1))+1;

    span = nIn-1;
    step = span/(nInt-1);

    stepOut = span/(n-1);
    incOut = uint32(stepOut/step);

    xOut = 1:step:nIn;
    xIn = 1:nIn;

    sCMRmap(1:nInt,1:3)=zeros;
    aCMRmap(1:n,1:3) = zeros;

    for i = 1:3
        sCMRmap(:,i) = interp1(xIn,CMRmap(:,i),xOut,'linear');
    end % for

    sCMRmap = abs(sCMRmap/max(max(sCMRmap)));

    for i = 1:3
        for a = 1:n
            aCMRmap(a,i) = sCMRmap(uint32(1+(incOut*(a-1))),i);
        end % for
    end % for
    
    %% If input m is specified then return just the one colour
    if nargin>1
        aCMRmap = aCMRmap(m, :);
    end % if

end % Function