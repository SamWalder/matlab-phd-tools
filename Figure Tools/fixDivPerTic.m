function fixDivPerTic(axesHandle)
%FIXDIVPERTIC Fixes the number of division per tick mark
%  This function when run will fix the number of divisions shown per tick
%  in a plot that has been generated using scopePlot. It will make it so
%  that there is one division per tick mark, as you would expect things to
%  be.
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * axesHandle - Handle to the axes to work on
%	Dependencies
%		* 
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

% Initilise some variables for later
minYDiv = 0;
maxYDiv = 0;

% Get the divisions per tick setting
yTicks = get(axesHandle, 'YTick');
divTick = yTicks(2) - yTicks(1);

% Find the lines in the axes
lines = findobj(axesHandle, 'type', 'line');

if divTick ~= 1
    % Run through each of the lines
    for lineCount = 1:length(lines);

        % Scale the data
        lineYData = get(lines(lineCount), 'yData');
        lineYData = lineYData / divTick;
        set(lines(lineCount), 'yData', lineYData);

        % Store the min and max of the yData for later use
        minYDiv = min([lineYData, minYDiv]);
        maxYDiv = max([lineYData, maxYDiv]);

        % Change the line description in the legend
            % Get the old description
            lineName = get(lines(lineCount), 'DisplayName');

            % Find the locations of the opening bracket and the space after
            % this
            openIdx = strfind(lineName, '(');
            closeIdx = openIdx + strfind(lineName(openIdx:end), ' ');

            % Convert the characters into a number
            oldScale = str2double(lineName(openIdx+1:closeIdx-1));

            % Work out what the new scale will be
            newScale = oldScale * divTick;

            % Work out the new string and apply it
            newLineName = [lineName(1:openIdx), num2str(newScale), lineName(closeIdx-1:end)];
            set(lines(lineCount), 'DisplayName', newLineName);

    end; % for

    % Now we have to manually fix the ticks
    set(axesHandle, 'YLim', [floor(minYDiv), ceil(maxYDiv)]);
    set(axesHandle, 'YTick', floor(minYDiv):ceil(maxYDiv));
    set(axesHandle, 'YTickLabel', cellfun(@num2str, num2cell(floor(minYDiv):ceil(maxYDiv)),  'UniformOutput', false));

end; % if