function truncateData( figure_handle )
%TRUNCATEDATA Truncates the data on a figure to that which is currently
%displayed
%   
%   Sam Walder - University of Bristol - 2013
%
% =========================================================================
%   Input arguments:
%       * figure_handle - a handle to the figure you want to run this on
%       (defaults to gcf)
%   Output arguments
%       * 
%
% =========================================================================

% Get the number of inputs and define the defults for missing inputs
if nargin < 1
    figure_handle = gcf;
end; % if

% Find all the axis within the current figure and store them
figure_children = get(figure_handle, 'children');
axis = zeros(1, length(figure_children));
no_axis = 0;
for i = 1:length(figure_children)
    if strcmp(get(figure_children(i), 'type'), 'axes')
        if ~strcmp(get(figure_children(i), 'tag'), 'legend')
            no_axis = no_axis + 1;
            axis(1, no_axis) = figure_children(i);
        end; % if
    end; % if
end; % for

axis = axis(1:no_axis);

% For each axis...
for i = 1:length(axis)
    % Find the x limits
    x_lim = get(axis(i), 'xlim');
    
    % Find all the line handles
    axis_children = get(axis(i), 'children');
    lines = zeros(1, length(axis_children));
    no_lines = 0;
    for a = 1:length(axis_children)
        if strcmp(get(axis_children(a), 'type'), 'line')
            no_lines = no_lines + 1;
            lines(1, no_lines) = axis_children(a);
        end; % if
    end; % for

    lines = lines(1:no_lines);
    
    % For each line...
    for b=1:no_lines
        % Get the lines xdata
        xdata = get(lines(b), 'xdata');
        
        % Convert the x limits to indicies
        if xdata(1) > x_lim(1)
            % The data does not run all the way to the end
            start_index = 1;
        else
            start_index = 1;
            while xdata(start_index) < x_lim(1)
                start_index = start_index + 1;
            end; % while
        end; % if
        
        if xdata(end) < x_lim(2)
            % The data does not run all the way to the end
            end_index = length(xdata);
        else
            % The data does run all the way to the end
            end_index = start_index;
            while xdata(end_index) < x_lim(2)
                end_index = end_index + 1;
            end; % while
        end; % if

        % Get the x and y data, return it to the line object truncated to
        % the range defined by the limits
        ydata = get(lines(b), 'ydata');
        
        set(lines(b), 'xdata', xdata(start_index:end_index));
        set(lines(b), 'ydata', ydata(start_index:end_index));
    
    end; % for

end;

