function tidyAxes(varargin)
%   Tidies the axes in a figure.
%   For linear axes:
%       Takes the axis to be adjusted and creates between 4 and 10 lables 
%       shifted to the intersection with the other axis and removes 
%       standard form from the notation adjusting the axis lable with SI
%       prefix.
%   For logarithmic axes:
%       Alters names of ticks on axis to aid redability
%
%   Sam Walder - University of Bristol - 2014
%
% =========================================================================
%   Input arguments:
%       * handle - (Optional) Argument that can either be a figure handle
%                  or and axis handle for the function to run on. Defaults
%                  to gcf
%       * action - (Optional) Axis to operate on. Options are:
%                       * 'all' - Operates on all
%                       * 'x'   - Horazontal axes only (default)
%                       * 'y'   - Verticle axes only
%   Output arguments:
%       * 
%
% =========================================================================
%   Change Log:
%       * 2014 - Created
%       * 2016 - Sam Walder
%           * Added a single input argument to act on a given handle
%       * 2017 - Sam Walder
%           * Merged the functionality of all of the previouse axes tidying
%             functions
%           * Removed the horribly unreadable code structures
%           * Chopped it up into functions to aid in the readability
%       * 2017 - Sam Walder
%           * Added rounding to the labels as the computer precision was
%             an issue
% =========================================================================

%% Input handling
% Deal with the given inputs
if nargin == 0
    % Nothing interesting happened, work on the current figure on the
    % horazontal axis
    inHandle = gcf;
    action = 'x';
elseif nargin == 1
    % We now need to see if we were passed a figure, an axes handel, or an
    % action
    if isa(varargin{1}, 'matlab.ui.Figure') || isa(varargin{1}, 'matlab.graphics.axis.Axes')
        % We have been given a figure OR axes handle, make other assumptions
        inHandle = varargin{1};
        action = 'x';
    elseif isa(varargin{1}, 'char')
        % We have been given the action.
        action = varargin{1};
        inHandle = gcf;
    else
        % Input error
        error('Something is wrong with the inputs');
    end;
elseif nargin == 2
    % We have fully defined arguments
    inHandle = varargin{1};
    action = varargin{2};
else
    % Wrong number of arguments
    error('Invalid number of input arguments');
end;

%% Create a vector of the axes handles
if isa(inHandle, 'matlab.ui.Figure')
    % If it is a figure then we need to pull out all of the children that
    % are axes
    children = get(inHandle, 'children');
    n = 1;
    axes = cell(1, length(children));
    for iCountChildren = 1:length(children);
        if isa(children(iCountChildren), 'matlab.graphics.axis.Axes')
            axes{n} = children(iCountChildren);
            n = n + 1;
        end; % if
    end; % for
    axes = axes(1:n - 1);
else
    % If it is not a figure, then it must be an axes handle
    axes{1} = inHandle;
end; % if

%% Run the appropriate axis formatting
switch action
    case 'x'
        doX = 1;
        doY = 0; 
    case 'y'
        doX = 0;
        doY = 1;
    case 'all'
        doX = 1;
        doY = 1;
    otherwise
        error('Unrecognised input');
end; % swich

if doX == 1
    if strcmp(get(axes{1}, 'XScale'), 'log')
        fixLogLabels('x');
    else
        prefix = setupLinearTicks(axes, 'x');
        createAxisLable(axes, prefix, 'x');
    end; % if
end; % if

if doY == 1
    if strcmp(get(axes{1}, 'YScale'), 'log')
        fixLogLabels('y');
    else
        prefix = setupLinearTicks(axes, 'y');
        createAxisLable(axes, prefix, 'y');
    end; % if
end; % if
    



end % function

%% Log axis laballing
function fixLogLabels(action)
% Gets the ticks from a log axis and creats nice labels for them
%   Input arguments:
%       * action      - 'x' or 'y' to act on appropriate axis
% =========================================================================
    
    % Get the ticks off the axis
    if strcmp(action, 'x')
        ticks = get(gca, 'XTick');
    elseif strcmp(action, 'y')
        ticks = get(gca, 'YTick');
    end; % if
    
    % Create a cell array for the labels
    labels = cell(length(ticks), 1);
    
    % Run through this and run the num2eng function on it
    for i=1:length(ticks)
        labels{i} = num2eng(ticks(i), 0);
    end % for
    
    % Apply the labels to the axis
    if strcmp(action, 'x')
        set(gca, 'XTickLabel', labels);
    elseif strcmp(action, 'y')
        set(gca, 'YTickLabel', labels);
    end; % if

end % function

%% Linear tick sorting
function prefix = setupLinearTicks(axesHandles, action)
% This is the bit that chopes up the axes into the right number of chunks
%   Input arguments:
%       * axesHandles - A structure of handles to the axes
%       * action      - 'x' or 'y' to act on appropriate axis
%   Output arguments:
%       * prefix      - The SI prefix that should be used with the units
% =========================================================================

    % Get the information needed for the clever bit to run
    % Offset is the point we would like to line up with the other axis 
    if strcmp(action, 'x')
        offset = axesHandles{1}.XLim(1);
        range = axesHandles{1}.XLim(2) - axesHandles{1}.XLim(1);
    elseif strcmp(action, 'y')
        offset = axesHandles{1}.YLim(1);
        range = axesHandles{1}.YLim(2) - axesHandles{1}.YLim(1);
    end; % if

    % Determin best division (Based on first axis found)

        % Get it in the range 5 < x < 100 (This just helps me think about it,
        % P.33 of my lab book shows the method)
        power = 0;
        scalledRange = range;
        while (scalledRange < 5) || (scalledRange >= 100)
            if range >= 100
                scalledRange = scalledRange/10;
                power = power + 1;
            else
                scalledRange = scalledRange * 10;
                power = power - 1;
            end % if
        end % while

        % Work out the preffered div spacing (1, 2, 5 or 10).
        % The preffered one is the biggest one that gives 4+ divisions
        if scalledRange / 50 >= 4
            tick_spacing = 50 * 10^power;
        elseif scalledRange / 20 >= 4
            tick_spacing = 20 * 10^power;
        elseif scalledRange / 10 >= 4
            tick_spacing = 10 * 10^power;
        elseif scalledRange / 5 >= 4
            tick_spacing = 5 * 10^power;
        elseif scalledRange / 2 >= 4
            tick_spacing = 2 * 10^power;
        elseif scalledRange / 1 >= 4
            tick_spacing = 1 * 10^power;
        else
            error('Something went wrong: the range of the x-axis does not fit into one of the preffered tick spacings')
        end % if

        % So how many ticks are there within the range?
        no_of_ticks = floor(range / tick_spacing) + 1;  % The +1 compensats for the tick at 0. I always forget that one

    % Calculate the tick points
        new_Tick = zeros(1, no_of_ticks);
        for j = 1:no_of_ticks
            new_Tick(1, j) = offset + tick_spacing*(j-1);    % Lo-lim + some number of divisions
        end % for

    % Figure out the lables
        factor = 3*(floor(power / 3));

        switch factor
            case -18
                prefix = 'a'; % atto-
            case -15
                prefix = 'f'; % femto-
            case -12
                prefix = 'p'; % pico-
            case -9
                prefix = 'n'; % nano-
            case -6
                prefix = '\mu'; % micro-
            case -3
                prefix = 'm'; % milli-
            case 0
                prefix = '';
            case 3
                prefix = 'k'; % kilo-
            case 6
                prefix = 'M'; % mega-
            case 9
                prefix = 'G'; % giga-
            case 12
                prefix = 'T'; % tera-
            case 15
                prefix = 'P'; % peta-
            case 18
                prefix = 'E'; % exa-
            otherwise
                error('Scaling factor is not within the defined range of SI prefixis avalible to this program, either add the prefix to the switch statment or revise the numbers on your Axis');
        end % switch

        new_TickLabel = cell(no_of_ticks,1);
        for j = 1:no_of_ticks
            new_TickLabel{j,1} = round((new_Tick(1,j)-offset)/(10^(factor)));   
        end % for
        
    % Apply the calculated ticks and labels to the axis
        for i=1:length(axesHandles)
            if i==1
                if strcmp(action, 'x')
                    set(axesHandles{i}, 'XTick', new_Tick);
                    set(axesHandles{i}, 'XTickLabel', new_TickLabel);
                elseif strcmp(action, 'y')
                    set(axesHandles{i}, 'YTick', new_Tick);
                    set(axesHandles{i}, 'YTickLabel', new_TickLabel);
                end; % if
            else
                % Put in some blank tick labels
                if strcmp(action, 'x')
                    set(axesHandles{i}, 'XTick', new_Tick);
                    set(axesHandles{i}, 'XTickLabel', {});
                elseif strcmp(action, 'y')
                    set(axesHandles{i}, 'YTick', new_Tick);
                    set(axesHandles{i}, 'YTickLabel', {});
                end; % if
            end; % if
        end % for

end % Function

%% Create axis label
function createAxisLable(axesHandles, prefix, action)
% This is the bit that comes up with what the axis label should be
%   Input arguments:
%       * axesHandles - A structure of handles to the axes
%       * prefix      - The SI prefix that should be used with the units
%       * action      - 'x' or 'y' to act on appropriate axis
% =========================================================================

    % Get the current label from the correct one of the axes
    if strcmp(action, 'x')
        currentLabel = get(axesHandles{1}.XLabel, 'String');
    elseif strcmp(action, 'y')
        currentLabel = get(axesHandles{1}.YLabel, 'String');
    end; % if

    % Decide what to do with the current label
    if strcmp(currentLabel, '')
        % If there is no label then we make up the details
        currentUnit = 's';
        currentName = 'Time';

    else
        % Check if it has already got a unit scalor on the end
        % Look for a close bracket on the end first
        if strcmp(currentLabel(end),')')
            % If we have found a close bracket then we need to find out
            % where the opening one is
            openIndex = strfind(currentLabel, '(');

            % Just use the last open bracket if more than one is found
            if length(openIndex) > 1
                openIndex = openIndex(end);
            end; % If

            % Get the current label without the units
            currentName = currentLabel(1:openIndex-2);

            % Store the current unit
            currentUnit = currentLabel(openIndex+1:end-1);

        else
            % No bracket detected, so assume there is not currently a unit.
            % Assume that it is time
            currentUnit = 's';

        end; % if        
    end % if
    
    % Check to see if the unit already has a prefix on it
    prefixes = {'a', 'f', 'p', 'n', '\mu', 'm', 'k', 'M', 'G', 'T', 'P', 'E'};
    if (length(currentUnit)>1)
        for iCountTwo = 1:length(prefixes)
            if strfind(currentUnit, prefixes{iCountTwo})
                nChar = length(prefixes{iCountTwo});
                currentUnit = currentUnit(nChar+1:end);
            end; % if
        end; % for
    end; % if

    % Now assembe a new label        
    new_Label = strcat(currentName, ' (', prefix, currentUnit, ')');

    % Apply these things to the axis
    for i=1:length(axesHandles)
        if i==1
            if strcmp(action, 'x')
                set(axesHandles{i}.XLabel, 'String', new_Label);
            elseif strcmp(action, 'y')
               set(axesHandles{i}.YLabel, 'String', new_Label);
            end; % if
        else
            if strcmp(action, 'x')
                set(axesHandles{i}.XLabel, 'String', '');
            elseif strcmp(action, 'y')
               set(axesHandles{i}.YLabel, 'String', '');
            end; % if
        end % if
    end % for
        
end % Function