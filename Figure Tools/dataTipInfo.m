function dataTipInfo()
%   Gets information from two datatips placed on a figure
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

% Get all the tips from the figure
tips = findall(gcf, 'Type', 'hggroup');

% Check for the right number of tips
if length(tips) ~= 2
    error('This only works with two data tips');
end; % if
    
% Get the time difference between the two data tips
deltaTime = abs(tips(2, 1).Position(1) - tips(1, 1).Position(1));
vFrequency = 1/deltaTime;
deltaAmp = abs(tips(2, 1).Position(2) - tips(1, 1).Position(2));
disp(['Period = ', num2eng(deltaTime), 's']);
disp(['Frequency = ', num2eng(vFrequency), 'Hz']);
disp(['Delta Amp = ', num2eng(deltaAmp)]);