function [f, ca, o] = spider(data, limits, lbl, leg, f)
% create a spider plot for ranking the data
%
% inputs  6 - 5 optional
% data    input data (NxM) (# axes (M) x # data sets (N))     class real
% rng     peak range of the data (Mx1 or Mx2)                 class real
% lbl     cell vector of axes names 
% leg     data set legend identification (1xN)                class cell
% f       figure handle or plot handle                        class real
%
% outptus 3 - 3 optional
% f       figure handle                                       class integer
% x       axes handle                                         class real
% o       series object handles                               class real
%
% michael arant - jan 30, 2008
% Modifications by:
%   Sam Walder - 06/2017 - Univeristy of Bristol - sam.walder@bristol.ac.uk
%   -> Fixed code issues
%   -> Updated color function to my prefered map
%   -> Changed all the fonts to TimesNewRoman
%   -> Removed the title
%   -> Made the axes and tick handles invisible
%   -> Gave variables human readable names
%   -> Combined related variables into the main data structure 'pData'
%   -> Changed the 'range' input into a 'limits' input
%   -> Made sure that all of the original data is saved into the user data
%      of the axes
%   Sam Walder - 06/2017 - Univeristy of Bristol - sam.walder@bristol.ac.uk
%   -> Adjusted how the axis labels are detected
%   -> Fixed a large number of misuses of nAxis and nDataSets
%
% to skip any parameter, enter null []
% 
% examples
% 
%  	spider([1 2 3; 4 5 6; 7 8 9; 10 11 12; 13 14 15;16 17 18; ...
%  	19 20 21; 22 23 24; 25 26 27]','test plot');
% 
%  	spider([1 2 3 4; 4 5 6 7; 7 8 9 10; 10 11 12 13; 13 14 15 16; ...
%  	16 17 18 19; 19 20 21 22; 22 23 24 25; 25 26 27 28],'test plot', ...
%  	[[0:3:24]' [5:3:29]'],[],{'Case 1' 'Case 2' 'Case 3' 'Case 4'});
% 
%  	spider([1 2 3 4; 4 5 6 7; 7 8 9 10; 10 11 12 13; 13 14 15 16; ...
%  	16 17 18 19; 19 20 21 22; 22 23 24 25; 25 26 27 28],'test plot', ...
%  	[],[],{'Case 1' 'Case 2' 'Case 3' 'Case 4'});

%% Input handling
% data check
if nargin < 1; help spider; error('Need data to plot'); end

% size segments and number of cases
[nDataSets, nAxis] = size(data);
% exit for too few axes
if nDataSets < 3
	errordlg('Must have at least three measuremnt axes')
	error('Program Termination:  Must have a minimum of three axes')
end

% Put the data into the main structure
for setCount = 1:nDataSets
    pData(setCount).yData = data(setCount, :);
end; % For

% Calculate the range on each axis
for axisCount = 1:nAxis
    % Initilise some numbers
    rangeLo = pData(1).yData(axisCount);
    rangeHi = pData(1).yData(axisCount);
    for pointCount = 1:nDataSets
        rangeLo = min([pData(pointCount).yData(axisCount), rangeLo]);
        rangeHi = max([pData(pointCount).yData(axisCount), rangeHi]);
    end; % for
    axesData(axisCount).range = [rangeLo, rangeHi];
end; % For

% Check for axis labels
if ~exist('lbl','var') || isempty(lbl)
	% no labels given - define a default lable set
	lbl = cell(nAxis,1); for ii = 1:nAxis; lbl(ii) = cellstr(sprintf('Axis %g',ii)); end
elseif length(lbl) ~= nAxis
    % They have been defined, but there are the wrong numbe of them
    error('%g axis labels defined, %g axes exist', length(lbl), nAxis);
else
    % The corrent number of axis labels have been definer, do nothing
end
% Put these in the axes structure
[axesData(:).label] = lbl{:};

% Check for data set labels
if ~exist('leg', 'var') || isempty(leg)
	% no data legend - define default legend
	leg = cell(1,nDataSets); 
    for ii = 1:nAxis 
        leg(ii) = cellstr(sprintf('Set %g',ii)); 
    end
elseif numel(leg) ~= nDataSets
	error('%g data sets labeled, %g exist', numel(leg), nDataSets)
end
% Store these in the data structure
[pData(:).label] = leg{:};


%% Check for figure or axes
if ~exist('f', 'var')
	% no figure or axes specified - create new ones
	f = figure; 
    ca = gca(f); 
    cla(ca); 
    hold on; 
    set(f, 'color', 'w')
    
elseif isa(f, 'matlab.ui.Figure')
	% existing figure specified - clear and set up
	ca = gca(f); 
    hold on;
    
elseif isint(f)
	% generating a new figure
	figure(f); 
    ca = gca(f); 
    cla(ca); 
    hold on;
    
else
    % Make new figure and axes
    disp('Invalid axes handle passed.  Generating new figure')
    f = figure; 
    ca = gca(f); 
    cla(ca); 
    hold on;
end

%% Fiddle with the figure?
% set the axes to the current text axes
axes(ca)
% set to add plot
set(ca,'nextplot','add');

% clear figure and set limits
set(ca,'visible','off'); 
set(f,'color','w');
set(ca,'xlim',[-1.25 1.25],'ylim',[-1.25 1.25]); 
axis(ca,'equal','manual');


%% Setup tick positions
for axisCount = 1:nAxis
    % Us my fantastic new bodgey function. The last number will control the
    % label density
    [axesData(axisCount).tickLocations] = ticksFromLims(axesData(axisCount).range, 6);
end; % For


%% Setup some Y Limits
% First check if the user botherd to define them
if exist('limits', 'var') && ~isempty(limits)
    % User has defined the Y limits, put them into the structure
    axesData(:).YLims = limits;
else
    % The user has not defined them, so we need to choose them
    % Use the ones that came out of doing the tick locations
    for axisCount = 1:nAxis
        [axesData(axisCount).yLims] = [axesData(axisCount).tickLocations(1), axesData(axisCount).tickLocations(end)];
    end; % For
end; % If


%% Data scaling and clipping
% Scale the data so that it fits in the range 0:1
for axisCount = 1:nAxis
    for setCount = 1:nDataSets
        sampleValue = pData(setCount).yData(axisCount);
        yLimLo = axesData(axisCount).yLims(1);
        yRange = axesData(axisCount).yLims(2) - yLimLo;
        scaledSample = (sampleValue - yLimLo) / yRange;
        
        % Clip values greater than 1 or less than 0
        if scaledSample > 1
            scaledSample = 1;
        end; % If
        if scaledSample < 0
            scaledSample = 0;
        end; % If
        
        pData(setCount).yDataScaled(axisCount) = scaledSample;
    end; % For
        
    % Scale the tick locations
    tickLocations = axesData(axisCount).tickLocations;
    yLimLo = axesData(axisCount).yLims(1);
    yRange = axesData(axisCount).yLims(2) - yLimLo;
    axesData(axisCount).scaledTickLocations = (tickLocations - yLimLo) ./ yRange;

end; % For

% Rotate the data
for setCount = 1:nDataSets
    pData(setCount).yDataScaled = pData(setCount).yDataScaled';
end; % For

%% Wrap data (close the last axis to the first)
% This adds a copy of the first axis so that we end up with a line segment
% between the last axis and the first
xAnglesWrapped = linspace(0, 2*pi, nAxis+1)';
% Save these in the axes structure
for iCount = 1:nAxis
    [axesData(iCount).theta] = xAnglesWrapped(iCount);
end; % For


%% Make the plot
% define the axis locations
xAngles = [axesData(:).theta];
start = [zeros(1, nAxis); cos(xAngles)]; 
stop = [zeros(1, nAxis); sin(xAngles)];

% plot the axes
plot(ca, start, stop, 'color', 'k', 'linestyle', '-', 'DisplayName', 'Axis', 'HandleVisibility', 'off'); 
axis equal

%% Plot the tick marks and labels
% Appearance settings
markerLength = .025; 
textOffest = 4 * markerLength;

for ii = 1:nAxis
    % Load tick positions
    tickPositions = axesData(ii).scaledTickLocations; 
    
	% Plot tick marks
    for tickCount = 1:length(tickPositions)
        x1 = (cos(axesData(ii).theta) * tickPositions(tickCount) + sin(axesData(ii).theta) * markerLength);
        x2 = (cos(axesData(ii).theta) * tickPositions(tickCount) - sin(axesData(ii).theta) * markerLength);
        y1 = (sin(axesData(ii).theta) * tickPositions(tickCount) - cos(axesData(ii).theta) * markerLength);
        y2 = (sin(axesData(ii).theta) * tickPositions(tickCount) + cos(axesData(ii).theta) * markerLength);
        plot(ca, [x1; x2], [y1; y2], 'color', 'k', 'displayName', 'tick', 'HandleVisibility', 'off');
    end; % For
    
	% Label the tick marks
    for jj = 1:length(tickPositions)
        xPos = (cos(axesData(ii).theta) * tickPositions(jj) + sin(axesData(ii).theta) * textOffest);
        yPos = (sin(axesData(ii).theta) * tickPositions(jj) - cos(axesData(ii).theta) * textOffest);
        textString = num2str(axesData(ii).tickLocations(jj));
        textHandle = text(xPos, yPos, textString, 'fontsize', 8, 'FontName', 'Times New Roman');
        % flip the text alignment for lower axes
        if axesData(ii).theta >= pi
            set(textHandle, 'HorizontalAlignment', 'right');
        end
    end
    
	% Label each axis
	temp = text((cos(xAngles(ii)) * 1.1 + sin(xAngles(ii)) * 0), ...
                (sin(xAngles(ii)) * 1.1 - cos(xAngles(ii)) * 0), ...
                char(axesData(ii).label),...
                'fontsize', 12, 'FontName', 'Times New Roman');
	% flip the text alignment for right side axes
	if xAngles(ii) > pi/2 && xAngles(ii) < 3*pi/2
		set(temp,'HorizontalAlignment','right')
	end
end


%% Plot the data
for setCount = 1:nDataSets
    yValues = [pData(setCount).yDataScaled];
    yValuesWrapped = [yValues; yValues(1)];
    o(setCount) = polar(ca, xAnglesWrapped, yValuesWrapped);
end; % For

% Set color of the lines
for ii = 1:nDataSets 
    set(o(ii), 'color', cmrmap(nDataSets, ii), 'linewidth', 1.5); 
end

%% Apply the legend
legend(o, leg, 'location', 'best', 'FontName', 'Times New Roman');


%% Save the data structures into the user data so that we can get back to them
plotData.pData = pData;
plotData.axesData = axesData;
set(gca, 'Userdata', plotData);

return

function [v] = rd(v,dec)
% quick round function (to specified decimal)
% function [v] = rd(v,dec)
%
% inputs  2 - 1 optional
% v       number to round    class real
% dec     decimal loaction   class integer
%
% outputs 1
% v       result             class real
%
% positive dec shifts rounding location to the right (larger number)
% negative dec shifts rounding location to the left (smaller number)
%
% michael arant
% Michelin Maericas Research and Development Corp
if nargin < 1; help rd; error('I/O error'); end

if nargin == 1; dec = 0; end

v = v / 10^dec;
v = round(v);
v = v * 10^dec;

function [res] = isint(val)
% determines if value is an integer
% function [res] = isint(val)
%
% inputs  1
% val     value to be checked              class real
%
% outputs 1
% res     result (1 is integer, 0 is not)  class integer
%
% michael arant     may 15, 2004
if nargin < 1; help isint; error('I / O error'); end

% numeric?
if ~isnumeric(val); error('Must be numeric'); end

% check for real number
if isreal(val) && isnumeric(val)
%	check for integer
	if round(val) == val
		res = 1;
	else
		res = 0;
	end
else
	res = 0;
end

