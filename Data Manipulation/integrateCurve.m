function [area, start, stop, range, peak] = integrateCurve(line_handle, start_threshold, start_offset, stop_threshold, floor)
%INTEGRATECURVE Integrates a curve
%   This function integates a curve taken from a figure. It automatically
%   calculates the points to integrate between on the defined curve. It
%   also shades the area that was integrated.
%
%   Sam Walder - University of Bristol - 2014
%
% =========================================================================
%   Input arguments:
%       * line_handle       - A handle to the line you want to use (use gco)
%       * start_threshold   - The threshold to start the integration
%       * start_offset      - The point to start looking from (use 0)
%       * stop_threshold    - The threshold to stop the integration
%       * floor             - The floor to use for the integration, usually 0
%   Output arguments
%       * area      - The calculated area
%       * start     - The start point that was calculated
%       * stop      - The stop point that was calculated
%       * range     - The range over which the integration was perfomed
%       * peak      - The peak date value that was encountered
%   Dependancies
%       * jbfill    - Allows filling of the area integrated
%
% =========================================================================
%   Change Log:
%       * 2014 - Created
%       * 2017 - Sam Walder
%           * Added the start_offset option
% =========================================================================

%% Get the data from the figure
XData = get(line_handle, 'XData');
YData = get(line_handle, 'YData');

color = get(line_handle, 'Color');

%% Get the index from which we start searching
startIndex = round(interp1(XData, 1:length(XData), start_offset));

%% Find the start and stop points
while YData(startIndex) < start_threshold
    if startIndex < length(YData)
        startIndex = startIndex+1;
    else
        error('start_threshold is not crossed at any point in the defined dataset');
    end % if
end % while

start = XData(startIndex);

stopIndex = startIndex + 1;
while YData(stopIndex) > stop_threshold
    if stopIndex < length(YData)
        stopIndex = stopIndex+1;
    else
        break;
    end % if
    
end % while

stop = XData(stopIndex);

range = stop - start;

%% Integrate
area = trapz(XData(startIndex:stopIndex), YData(startIndex:stopIndex) ) - range*floor;

%% Shade the realvent area
jbfill(XData(startIndex:stopIndex), YData(startIndex:stopIndex), ones(1,length(YData(startIndex:stopIndex)))*floor, color, color, 1, 0.5)

%% Calculate other outputs
peak = max(YData);

end

