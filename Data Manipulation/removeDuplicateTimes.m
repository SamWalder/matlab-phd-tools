function [strippedTime, strippedWaveform] = removeDuplicateTimes(time, waveform)
%REMOVEDUPLICATETIMES - Removes any time samples that have been repeated in
%the data
%
%   Sam Walder - University of Bristol - 2016 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments:
%       * time             - Time vector with repeated samples
%       * waveform         - The samples of a waveform to go with time
%   Output arguments
%       * strippedTime     - Time vector with the duplicated removed
%       * strippedWaveform - Accompanying waveform samples
%	Dependencies
%		*
% =========================================================================
%   Change Log:
%       * 2016 - Created
% =========================================================================

%% Fist find the indicies of the duplicates
nDuplicates = 0;

for i=1:length(time)-1
    if time(i) == time(i+1)
        nDuplicates = nDuplicates + 1;
        duplicateIndicies(nDuplicates) = i;
    end
end

%% Strip these out of the data
% Go backwards through it so that we do not try and referece indicies of
% things we have already removed
for i=0:nDuplicates-1
    time(duplicateIndicies(nDuplicates-i)) = [];
    waveform(duplicateIndicies(nDuplicates-i)) = [];
end

strippedTime = time;
strippedWaveform = waveform;

end

