function [x_frequency,y_amplitude] = samfft(x_time, y_amplitude, varargin)
%SAMFFT Takes a time domian waveform and its time axis and produces the
%frequency domian vectors
%
%   Sam Walder - University of Bristol - 2013 - sam.walder@bristol.ac.uk
%   Apollo Charalambous - University of Bristol - 2015 - apollo.charalambous@bristol.ac.uk
%
%   * NOTE: You may wish to redefine how custom_step is defined as this
%           will controll the maximum frequency that is used in the case that
%           intopolation occores
%
% =========================================================================
%   Input arguments:
%       * x_time        - The time domain time axis
%       * y_amplitude   - The amplitude vector associated with X
%       * remove_zeros  - Set to 1 to remove zero values from the results.
%                         This helps with producing loglog plots as zeros cause errors
%                         Default is not to (0)
%       * window        - Window to use.
%                         Acceptable values are: 'flattop' 'hamming'
%                         'hann' 'blackman' 'none'
%                         Default is none, this argument does not need to
%                         be specified.
%       * correction    - Correction factor to use with the window.
%                         You can either correct for the amplitude or the
%                         energy. 
%                         Valid entris are: 'none' 'amplitude' 'energy'
%   Output arguments
%       * x_frequency   - Frequency axis
%       * y_amplitude   - Amplitude axit to go with x_frequency
%	Dependencies
%		* num2eng       - Converts anumber into text with SI multiplier
%                         Can replace with num2str
% =========================================================================
%   CHANGE LOG:
%       * 2013 - Created
%       * 2015 - Apollo Charalambous - apollo.charalambous@bristol.ac.uk
%           Added the x2 multiplication
%       * 2015 - Sam Walder
%           Added remove zeros option
%           Fixed a bug in the remove zeros function
%       * 2015/11 - Sam Walder
%           Added the interpolation and windowing
%       * 2016/01 - Sam Walder
%           Fixed an error with line 187 (strcomp -> strcmp)
%       * 2016/02 - Sam Walder
%           Fixed bug causing interpolation to run unneceseraly
%       * 2016/06 - Sam Walder
%           Added input orientation auto corrention
%       * 2016/07 - Sam Walder
%           Fixed an issue in the maths whereby it would work out the wrong
%           frequency axis (used L instead of n)
% =========================================================================
%   EXAMPLES:
%       * Just do the DFT
%           [frequency, amplitudes] = samfft(time, amplitudes)
%       * Do the DFT and remove zeros
%           [frequency, amplitudes] = samfft(time, amplitudes, 1)
%       * Do the DFT with a window
%           [frequency, amplitudes] = samfft(time, amplitudes, 'hann')
%       * Do the DFT with a window and correct for amplitude
%           [frequency, amplitudes] = samfft(time, amplitudes, 'hann', 'amplitude')
%       * Do the DFT removing zeros and apppling a window
%           [frequency, amplitudes] = samfft(time, amplitudes, 1, 'hann')
%       * Do the DFT removing zeros and apppling a window and correcting
%         for energy
%           [frequency, amplitudes] = samfft(time, amplitudes, 1, 'hann', 'energy')
% =========================================================================

%% Decide what to do with the inputs
if nargin == 2
    % Only 2 inputs specified, assume defaults for others
    remove_zeros = 0;
    window = 'none';
    correction = 'none';
    
elseif nargin == 3
    % They have specified something else. Is it a string or a number?
    % If it is a string it must be the window
    % If it is an int then it must be zeros removal
    if ischar(varargin{1})
        % The 3rd input is a char array, they want a window
        window = varargin{1};
        remove_zeros = 0;
    else
        % The 3rd input must indicate if they want zeros removed
        if varargin{1} == 1
            remove_zeros = 1;
        elseif varargin{1} == 0
            remove_zeros = 0;
        else
            error('Invalid final argument, I accept 0, 1 or nothing');
        end; % if
    end; % if
    
    correction = 'none';
    
elseif nargin == 4
    % There are 2 unknown things specified. These could either be zeros and
    % a window OR window and correction.
    % If the first input is string then it is the latter
    
    if ischar(varargin{1})
        % The 3rd input is a character array.
        % They have specified a window and a correction
        window = varargin{1};
        correction = varargin{2};
        remove_zeros = 0;   % Default
    else
        % They must have specified the zeros and a window
        remove_zeros = varargin{1};
        window = varargin{2};
        correction = 'none';
    end; % if
    
elseif nargin == 5
    % Wow! they specified everything? Easy!
    remove_zeros = varargin{1};
    window = varargin{2};
    correction = varargin{3};
        
else
    % Cause an error
    error('Invalid number of arguments');
end; % if

%% Make sure the inputs are row vectors
sizeX = size(x_time);
if sizeX(1)>sizeX(2)
    x_time = x_time';
end; % if

sizeY = size(y_amplitude);
if sizeY(1)>sizeY(2)
    y_amplitude = y_amplitude';
end; % if


%% If the timbase is not uniform we need to do some intopolation of the data
% To check we need to see if the diff() of the timebase is a constant value
diff_time = diff(x_time);
accuracy = 1e-14;      % This gets us round the precision limits
if (all( (diff_time > (diff_time(1)-accuracy)) ) && all( (diff_time < (diff_time(1)+accuracy)) ))
    % They are all equal, this is good as we don't need to do anything
else
    % They are not all equal. This presents issues.
    % First find the minimum time step - we will use this as the time step
    % for our evenly spaced data (and issue a warning to the user about
    % this)
    warning('x_time is not uniform in step size - samfft will interpolate the data onto the custom step size. High frequency information will have been partially extrapolated!');
    min_step = min(diff_time);
    max_step = max(diff_time);
    mode_step = mode(diff_time);
    custom_step = mode_step/2;
    disp(['Extrapolation region: ', num2eng(round(1/max_step)), 'Hz to ', num2eng(round(1/custom_step)), 'Hz']);
    
    % Create a new uniform timebase
    uniform_time = linspace(x_time(1), x_time(end), range(x_time)/(mode_step/2) );
    
    % Intopolate the data
    uniform_amp = interp1(x_time, y_amplitude, uniform_time);
    
    % Overwrite the existing varaibles (Possibly not that efficent)
    x_time = uniform_time;
    y_amplitude = uniform_amp;
end; % if

%% Apply windowing
% Possible windows are: flattop | hamming | hann | blackman | none
switch window
    case 'none'
        % No window... do nothing
        window_amp = 1;
        
    case 'flattop'
        % Define a flattop window
        window_amp = rot90(flattopwin(length(y_amplitude)));
        
    case 'hamming'
        % Define a Hamming window
        window_amp = rot90(hamming(length(y_amplitude)));
        
    case 'hann'
        % Define a Hanning window
        window_amp = rot90(hann(length(y_amplitude)));
        
    case 'blackman'
        % define a Blackman Harris window
        window_amp = rot90(blackman(length(y_amplitude)));
        
    otherwise
        % They have not define a valid window. Tell them this! Make sure
        % they know they are wrong!
        error('The window type requested is not valid');
end; % switch

% Figure out the correction scaling values
amp_corr = 1/(sum(window_amp)/length(window_amp));        % (Mean) Scaling factor for amplitude
en_corr = 1/sqrt(sum(window_amp.^2)/length(window_amp));  % (RMS) Scaling factor for energy

% Apply this to the data
y_amplitude = y_amplitude .* window_amp;

%% Correct the amplitude or the power depending upon user input
if strcmp(correction, 'none')
    % Do nothing
elseif strcmp(correction, 'amplitude')
    y_amplitude = y_amplitude * amp_corr;
elseif strcmp(correction, 'energy')
    y_amplitude = y_amplitude * en_corr;
else
    error('The correction term requested is not recognised');
end; % if

%% Do the maths
% Set up a few useful things
L = length(y_amplitude);        % Number of samples

% Get the lenght to be the next power of 2
n = 2^nextpow2(L);

% Create Y axis
Y = fft(y_amplitude, n);
P2 = abs(Y/n);
P1 = P2(1:n/2+1);
P1(2:end-1) = 2*P1(2:end-1);
y_amplitude = P1;

% Figure out the time step
step = (x_time(end) - x_time(1)) / (length(x_time));

% Create X Axis
%x_frequency = 0: ((1/step)/2)/((n/2)-1) :(1/step)/2;
Fs = 1/step;
x_frequency = Fs*(0:(n/2))/n;

%% Deal with zeros
% Clear zeros if applicable
if remove_zeros == 1
    x_frequency = x_frequency(y_amplitude~=0);
    y_amplitude = y_amplitude(y_amplitude~=0);
end; % if

end

