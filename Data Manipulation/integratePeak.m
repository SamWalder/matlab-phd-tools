function [area, start, stop, range, peak] = integratePeak(line_handle, stop_threshold, floor)
%INTEGRATEPEAK Finds the peak of a curve and integrates around it
%   This function is an adaptation of the integrateCurve function by the
%   same autor. This version first finds the peak value of a curve and then
%   integrates either side of it down to the stop_threshold.
%
%   Sam Walder - University of Bristol - 2018
%
% =========================================================================
%   Input arguments:
%       * line_handle       - A handle to the line you want to use (use gco)
%       * stop_threshold    - The threshold to stop the integration
%       * floor             - The floor to use for the integration, usually 0
%   Output arguments
%       * area      - The calculated area
%       * start     - The start point that was calculated
%       * stop      - The stop point that was calculated
%       * range     - The range over which the integration was perfomed
%       * peak      - The peak date value that was encountered
%   Dependancies
%       * jbfill    - Allows filling of the area integrated
%
% =========================================================================
%   Change Log:
%       * 2018 - Created
% =========================================================================

%% Get the data from the figure
% Trim it to just the bit on screen
XData = get(line_handle, 'XData');
YData = get(line_handle, 'YData');
color = get(line_handle, 'Color');

XLims = get(gca, 'xlim');
leftClipIndex = round(interp1(XData, 1:length(XData), XLims(1), 'linear', 0));
rightClipIndex = round(interp1(XData, 1:length(XData), XLims(2), 'linear', length(XData)));

XData = XData(leftClipIndex:rightClipIndex);
YData = YData(leftClipIndex:rightClipIndex);

%% Find the index of the curves peak value
YData = cast(YData, 'double');
[pks,locs] = findpeaks(YData,'SortStr','descend');
peakIndex = locs(1);

%% Find the start and stop points
% Start index search
startIndex = peakIndex;
while YData(startIndex) > stop_threshold
    if startIndex > 0
        startIndex = startIndex-1;
    else
        error('start_threshold is not crossed at any point in the defined dataset');
    end % if
end % while

start = XData(startIndex);

% Stop index search
stopIndex = peakIndex;
while YData(stopIndex) > stop_threshold
    if stopIndex < length(YData)
        stopIndex = stopIndex+1;
    else
        break;
    end % if
    
end % while

stop = XData(stopIndex);

range = stop - start;

%% Integrate
area = trapz(XData(startIndex:stopIndex), YData(startIndex:stopIndex) ) - range*floor;

%% Shade the realvent area
jbfill(XData(startIndex:stopIndex), YData(startIndex:stopIndex), ones(1,length(YData(startIndex:stopIndex)))*floor, color, color, 1, 0.5)

%% Calculate other outputs
peak = max(YData);

end