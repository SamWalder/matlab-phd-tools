function SigGen_LoadWaveform_33220A(handle, timebase, waveform, varargin)
%SIGGEN_LOADWAVEFORM loads the vector waveform into the siggen
%   This is a new versions that has been updated to work with the Agilent
%   33220A.
%
%   Sam Walder - University of Bristol - 2016 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments:
%       * handle    - Handle to the signal genorator object
%       * timebase  - Timebase along which the signal should run
%       * waveform  - Waveform described as a vector
%       * safetyDisable - Optional, set to 666 to disable the question
%                         dialogue
%   Output arguments
%       * 
%	Dependencies
%		*
% =========================================================================
%   Change Log:
%       * 2014 - Created
%       * 2016 - Sam Walder
%           Created this updated version
% =========================================================================

% Signal genorator specification for aribirary mode
sigGenSpec.frequency = [1e-6, 6e6];     % Hz
sigGenSpec.wavLength = [2, 64e3];       % Points
sigGenSpec.ampRes = 14;                 % Bits
sigGenSpec.fs = 50e6;                   % Samples/second
% Note on samples/second: You can actually give the signal genorator a much
% higher rate than this if you want, but I gave it a go and there is no
% chance of it acheving it :(

% check if the last argument was given
if nargin > 3
    safetyDisable = varargin{end};
else
    safetyDisable = 0;
end; % if

% Allow Pausing
pause on;

% Ask the idiot operating the system that it is actually safe to do this
% now and is not going to blow up more IGBTs
if safetyDisable ~= 666
    % Construct a questdlg with three options
    choice = questdlg('Pressing OK will start the upload. The output of the signal genorator could be anything during this process. Is it safe to continue?', ...
        'Warning!', ...
        'Yes', 'No', 'Yes');
    % Handle response
    switch choice
        case 'Yes'
            % Just allow it to carry on
        otherwise
            % Stop it all
            error('User terminated operation');
    end
elseif safetyDisable == 666
    % Do nothing, we skip the safety check
end; % if

% Set the screen of the function genorator
fprintf(handle, 'display:text "Upload in Progress"');

% Start a timer
tic;

% Set up a loading bar
loadBox = waitbar(0,'Uploading', 'CreateCancelBtn', 'setappdata(gcbf,''canceling'',1)');
setappdata(loadBox,'canceling', 0);

% Turn the output off
fprintf(handle, 'OUTPut OFF');

% Clear the instrument
fprintf(handle, '*CLS');

% This section deals with issues relating to the horizontal resolution of
% the machine
    % Setup the frequency if it is achevable
    targetFrequency = 1/(timebase(end)-timebase(1));
    if targetFrequency < max(sigGenSpec.frequency)
        % Set it as such
        fprintf(handle, ['FREQuency ', num2str( targetFrequency )]);
    else
        % It is not achevable, so produce and error
        error('Given waveform period is too short and requres a function genorator frequency higher than supported. You could try padding the end of your waveform data with zeros to make the period longer.');
    end; % if
    
    % Check if the maximum resolution is achevable
    maxRes = min(diff(timebase));
    if maxRes >= 1/sigGenSpec.fs
        % This is achevable, so use it
        setRes = maxRes;
    else
        % Can't be done, time for a warning
        beep;
        warning('The smallest timestep in the provided timebase is smaller than what can be achevied with your signal genorator. Using maximum allowable. Some fidelity will be lost!');
        setRes = 1/sigGenSpec.fs;
    end; % if
    
    % Check if the number of points is achevible
    requestedPoints = (timebase(end)-timebase(1)) / setRes;
    if requestedPoints < max(sigGenSpec.wavLength)
        % All is well, we will use this many points
        setPoints = requestedPoints;
    else
        % All is not well, we will have to subsample the waveform
        beep;
        warning('You have defined a waveform the requires more points than the signal genorator supports. Using the maximum avalible. Fidelity will be reduced!');
        setPoints = max(sigGenSpec.wavLength);
    end; % if

    % Interppolate the data onto a new time axis using the number of points
    % defined above
    newTimebase = linspace(timebase(1), timebase(end), setPoints);
    % remove any duplicates in the existing timebase
    [strippedTime, strippedWaveform] = removeDuplicateTimes(timebase, waveform);
    newWaveform = interp1(strippedTime, strippedWaveform, newTimebase);
    

% Scale the data so that it fills the vertical range
dataMax = max(newWaveform);
dataMin = min(newWaveform);
dataRange = dataMax - dataMin;
dataOffset = dataMax - dataRange/2;             % The offset of the data
fullRange = (2^(sigGenSpec.ampRes-1)) * 2 - 2;  % The full range that we would like this to sit in
scalingFactor = fullRange / dataRange;
newWaveform = newWaveform * scalingFactor;
% Centre the data in the ADC
newWaveform = newWaveform - (dataOffset*scalingFactor);

% Set offset to zero before trying anything to try and get round settings
% conflict issues
fprintf(handle, ['VOLT:OFFS ', num2str(0)]);

% Now set the appropriate output votage
% This will effectivly scale the ADC values
fprintf(handle, ['VOLT ', num2str(dataRange), ' Vpp']);

% Now set the output offset we need
fprintf(handle, ['VOLT:OFFS ', num2str(dataOffset)]);

% Check for errors before we begin the upload
getErrors(handle, loadBox, '');

% Load data into the siggen
set(handle, 'EOIMode', 'off'); % Disable the End Or Identify (EOI) line so that commands can run on
fprintf(handle,'%s','DATA:DAC VOLATILE, ');
for i=1:length(newWaveform),
    fprintf(handle,'%s',num2str(newWaveform(i)));
    if (i ~= length(newWaveform)),
        fprintf(handle,'%s',',');
    end

    % Update the waitbar
    waitbar(i/length(newWaveform));
    
    % Check to see if the user is trying to cancel this
    if getappdata(loadBox, 'canceling')
        set(handle, 'EOIMode', 'on'); % Enable the End Or Identify (EOI) line to terminate the command
        fprintf(handle,'%s',' ');
        getErrors(handle, loadBox, 'User terminated upload');
    end
end
set(handle, 'EOIMode', 'on'); % Enable the End Or Identify (EOI) line to terminate the command
fprintf(handle,'%s',' ');

% Make sure it is outputting this function
fprintf(handle, 'FUNCTION USER');
fprintf(handle, 'FUNCtion:USER VOLATILE');

% Setup burst mode
if strcmp('33120A', SigGen_Model(handle))
    fprintf(handle,'BM:STATE ON');
elseif strcmp('33220A', SigGen_Model(handle))
    fprintf(handle,'BM:STATE ON');
    fprintf(handle,'BURSt:MODE TRIGgered');
elseif strcmp('33522B', SigGen_Model(handle))
    fprintf(handle,'BM:STATE ON');
    fprintf(handle,'BURSt:MODE TRIGgered');
else
    error('Failed to set burst mode');
end; % if

% Set the trigger source
fprintf(handle,'TRIGGER:SOURCE BUS');

% Set the output impeadance to 50 Ohms
fprintf(handle,'OUTPut:LOAD 50');

% See if we got any errors
getErrors(handle, loadBox, '');

% Turn the output on
fprintf(handle,'OUTPut ON');

% Alert that it is done
fprintf(handle, 'display:text "Upload Success"');
% Beep
for i=1:1
    fprintf(handle, 'SYST:BEEP');
    pause(0.2);
end; % for
% Pause and clear screen
pause(1);
fprintf(handle, 'display:text:CLEar');

disp(['Done! Elapsed time ', num2str(toc), ' seconds']);

% Remove the wait box
delete(loadBox);

end

function getErrors(SigGen, loadingBox, errorString)
% Get any errors from the device and display them to the user
% Adds the given error string to the message and closes the loading box
% If no errorstring is given then an error is not neciserily produced

% If an error string was passed then add it to the que
if ~strcmp(errorString, '')
    % An error has been passed
    errors{1} = strcat(errorString, '\n');
    errCount = 1;
else
    errCount = 0;
end; % if

% Have a look for any hardware errors
errors{errCount+1} = query(SigGen, 'SYSTem:ERRor?');
errCount = errCount + 1;
while ~strcmp(errors{end}(1:end-1), '+0,"No error"')
    errors{errCount+1} = query(SigGen, 'SYSTem:ERRor?');
    errCount = errCount + 1;
end; % while

% Discard the last message as this should be the "+0,"No error"" message
errCount = errCount-1;

% If we have errors stop the program
if errCount > 0
    % Put the error string together
    errorString = '';
    for i=1:errCount
        errorString = strcat(errorString, '\t->', {' '},  errors(i));
    end; % for
    errorString = strcat('You have', {' '}, num2str(errCount), ' errors:\n', errorString);
    
    % Append the time taken to this
    errorString = strcat(errorString, '\nElapsed time', {' '}, num2str(toc), ' seconds');
    
    % Get the siggen to display an error
    fprintf(SigGen, 'display:text "ERROR FOOL!"');
    % Beep
    fprintf(SigGen, 'SYST:BEEP');
    % Pause and clear screen
    pause(1);
    fprintf(SigGen, 'display:text:CLEar');
    
    % Clear the sig gen
    fprintf(SigGen, '*CLS');
    
    % Delete the loading box
    delete(loadingBox);
    
    % Print the errors
    error(sprintf(errorString{1}));
end; % if

end
