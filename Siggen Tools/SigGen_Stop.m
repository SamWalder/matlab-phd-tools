function SigGen_Stop(handle)
%SIGGEN_STOP Closes the handles communication channel and clears it from
%the workspace

% First close the connection
fclose(handle);

% Now find and delete the instrument handle
delete(instrfind('SerialNumber', handle.SerialNumber));


end