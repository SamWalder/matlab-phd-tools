function SigGen_Trig(handle)
%SIGGEN_TRIG Triggers the waveform

% Setup burst mode
if strcmp('33120A', SigGen_Model(handle))
    fprintf(handle,'BM:STATE ON');
    
elseif strcmp('33220A', SigGen_Model(handle))
    % If burst mode has been turned off we need to turn it on again
    if ~str2double(query(handle, 'BM:STATE?'))
        
        % Construct a questdlg
        choice = questdlg('The signal genorator is not in burst mode. This will be turned back on if you push yes, however a spike may come out of the generator when this happens. Make your circuit is safe and push ''Yes''', ...
            'Warning!', ...
            'Yes', 'Cancel', 'Cancel');
        
        % Handle the users response
        switch choice
            case 'Yes'
                % Fix the burst mode issue
                fprintf(handle,'BM:STATE ON');
                fprintf(handle,'BURSt:MODE TRIGgered');
            otherwise
                % Stop it all
                error('User terminated operation');
        end
        
        % Ask them to plug it back in
        choice = questdlg('Burst mode has been turned on, push ''Continue'' to continue with the trigger. Push ''Cancel'' to stop.', ...
            'Warning!', ...
            'Continue', 'Cancel', 'Cancel');
        
        % Handle the users response
        switch choice
            case 'Continue'
                % Just lets this program carry on
            otherwise
                % Stop it all
                error('User terminated operation');
        end
        
    end; % if

elseif strcmp('33522B', SigGen_Model(handle))
    % If burst mode has been turned off we need to turn it on again
    if ~str2double(query(handle, 'BM:STATE?'))
        
        % Construct a questdlg
        choice = questdlg('The signal genorator is not in burst mode. This will be turned back on if you push yes, however a spike may come out of the generator when this happens. Make your circuit is safe and push ''Yes''', ...
            'Warning!', ...
            'Yes', 'Cancel', 'Cancel');
        
        % Handle the users response
        switch choice
            case 'Yes'
                % Fix the burst mode issue
                fprintf(handle,'BM:STATE ON');
                fprintf(handle,'BURSt:MODE TRIGgered');
            otherwise
                % Stop it all
                error('User terminated operation');
        end
        
        % Ask them to plug it back in
        choice = questdlg('Burst mode has been turned on, push ''Continue'' to continue with the trigger. Push ''Cancel'' to stop.', ...
            'Warning!', ...
            'Continue', 'Cancel', 'Cancel');
        
        % Handle the users response
        switch choice
            case 'Continue'
                % Just lets this program carry on
            otherwise
                % Stop it all
                error('User terminated operation');
        end
        
    end; % if
else
    error('Failed to set burst mode');
end; % if

% Make sure the output is on
fprintf(handle,'OUTPut ON');

% Trigger
fprintf(handle,'*TRG');

end

