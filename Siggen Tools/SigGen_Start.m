function SigGen = SigGen_Start()
%SIGGEN_START Opens up the serial port for the siggen and gets it ready
%
%   Sam Walder - University of Bristol - 2014 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments:
%       * 
%   Output arguments
%       * SigGen - Handle to the signal genorator
%	Dependencies
%		* findInstrument - https://uk.mathworks.com/matlabcentral/fileexchange/25593-findinstrument-find-and-connect-to-instruments-using-the-model-or-manufacturer-name
% =========================================================================
%   Change Log:
%       * 2014 - Created
%       * 2016 - Sam Walder
%                Updated to allow conection to the newer USB type agilent
%                genorator
%       * 2016 - Sam Walder
%                Updated to automatically connect to any supported signal
%                genorator over any interface
% =========================================================================

% Set pausing to be allowed
pause on;

% Look for a Supported signal genorator
ins = findInstrument('33120A');
if isempty(ins)
    ins = findInstrument('33220A');
end; % if
if isempty(ins)
    ins = findInstrument('33522B');
end; % if


% If we found nothing make an error
if isempty(ins)
    error('I am really rather sorry, I have not been able to find anything');
end; % if

% If we found more than one then select the first
SigGen = ins(1);

% Connect to instrument object
fopen(SigGen);

% Call a reset
fprintf(SigGen, '*RST');

% Put the siggen in remote mode
fprintf(SigGen, 'system:remote');

% Show bootup text
fprintf(SigGen, 'display:text "Connection Success"');

% Beep
for i=1:3
    fprintf(SigGen, 'SYST:BEEP');
    pause(0.2);
end; % for

% Pause and clear screen
pause(1);
fprintf(SigGen, 'display:text:CLEar');

% Turn the output off
fprintf(SigGen, 'OUTPut OFF');


end

