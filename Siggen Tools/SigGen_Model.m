function [ model ] = SigGen_Model( handle )
%SIGGEN_MODEL returns the model of the SigGen
%   Returns the model of the siggen specified by handle as a string.
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * handle - The handle to an open siggen object
%   Output arguments
%       * modle - The model of the siggen

% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

% Ask for the identification of the instrument
idnback = query(handle, '*IDN?');

% Try and extract the model number from this string
if strfind(idnback, '33220A')
    % The model is 33220A
    model = '33220A';
elseif strfind(idnback, '33120A');
    model = '33120A';
elseif strfind(idnback, '33522B');
    model = '33522B';
else
    error('Cannot identify the model of the attached instrument');
end; % if

% That's all folks!


end

