% =========================================================================
% Agilent DSO7104A scaling data code - getXScalingData.m
% Niall Oswald September 2010 - no4208@bris.ac.uk
% =========================================================================
%
% This function returns parameters which enables the corresponding time
% vector to be assembled for a waveform. Also freq?
%
% Written for 7000 series, should also work with 6000 series.
%
% instrumentID is the instrument object corresponding to the scope.
% channelID is a numerical value (1-4) which specifies the channel to
% query.
%
% =========================================================================
% BEGIN CODE
% =========================================================================

function [xReference, xOrigin, xIncrement] = getXScalingData(instrumentID,channelID)

% Set up waveform source.
fprintf(instrumentID,':WAV:SOUR CHAN%d',channelID);

% Get CH1 scaling parameters - for convering raw 'scope ADC data to
% meaningful voltages.
xReference = str2double(query(instrumentID, ':WAV:XREF?'));     % Reference time
xOrigin = str2double(query(instrumentID, ':WAV:XOR?'));         % Origin (0 means far left of screen)
xIncrement = str2double(query(instrumentID, ':WAV:XINC?'));     % S/Step increment

% =========================================================================
% END CODE
% =========================================================================