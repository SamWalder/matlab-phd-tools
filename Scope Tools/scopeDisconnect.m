function scopeDisconnect(handle)
%SCOPEDISCONNECT Closes the handles communication channel and clears it from
%the workspace
%
%   Sam Walder - University of Bristol - 2016 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * handle - Handel to the instument to disconnect
% =========================================================================

% First close the connection
fclose(handle);

% Now find and delete the instrument handle
delete(instrfind('SerialNumber', handle.SerialNumber));

end