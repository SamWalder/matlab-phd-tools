function varargout = powerQuestionDlg(varargin)
% POWERQUESTIONDLG MATLAB code for powerQuestionDlg.fig
%
% Sam Walder - University of Bristol - 2017
%      
% Common function inputs: 
%       hObject    handle to figure
%       eventdata  reserved - to be defined in a future version of MATLAB
%       handles    structure with handles and user data (see GUIDATA)
%
        

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @powerQuestionDlg_OpeningFcn, ...
                       'gui_OutputFcn',  @powerQuestionDlg_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
end
    
% --- Executes just before powerQuestionDlg is made visible.
function powerQuestionDlg_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % varargin - command line arguments to powerQuestionDlg
    
    % Choose default command line output for powerQuestionDlg
    handles.output = hObject;

    % We are expecting to be passed an array of names for the waveforms
    if nargin > 3
        handles.waveformNames = varargin{1};
    else
        handles.waveformNames = {'Test Mode 1', 'Test Mode 2', 'Test Mode 3', 'Test Mode 4'};
    end
    
    % We may also be passed the selected options as an array
    if nargin > 4
        handles.defaultSelection = varargin{2};
    else
        % Set up some default choices
        handles.defaultSelection = [0, 0, 1, 1];
    end; % if

    % Update handles structure
    guidata(hObject, handles);

    initialize_gui(hObject, handles, false);

    % UIWAIT makes powerQuestionDlg wait for user response (see UIRESUME)
    uiwait(handles.figure1);
end


% --- Outputs from this function are returned to the command line.
function varargout = powerQuestionDlg_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);

    % Get default command line output from handles structure
    %varargout{1} = handles.output;

    % Read the state of each of the check boxes
    checkState = zeros(1, 4);
    checkState(1) = get(handles.Ch1, 'Value');
    checkState(2) = get(handles.Ch2, 'Value');
    checkState(3) = get(handles.Ch3, 'Value');
    checkState(4) = get(handles.Ch4, 'Value');

    % Pump out an array with the channel number of their choices
    varargout{1} = checkState;

    % Shut it down!!
    close;
end


function initialize_gui(fig_handle, handles, isreset)
    % If the metricdata field is present and the Cancel flag is false, it means
    % we are we are just re-initializing a GUI by calling it from the cmd line
    % while it is up. So, bail out as we dont want to Cancel the data.
    if isfield(handles, 'metricdata') && ~isreset
        return;
    end

    % Set the string names for the buttons
    set(handles.Ch1, 'String', handles.waveformNames{1});
    set(handles.Ch2, 'String', handles.waveformNames{2});
    set(handles.Ch3, 'String', handles.waveformNames{3});
    set(handles.Ch4, 'String', handles.waveformNames{4});
    
    % Set up the selected options
    set(handles.Ch1, 'Value', handles.defaultSelection(1));
    set(handles.Ch2, 'Value', handles.defaultSelection(2));
    set(handles.Ch3, 'Value', handles.defaultSelection(3));
    set(handles.Ch4, 'Value', handles.defaultSelection(4));

    % Update handles structure
    guidata(handles.figure1, handles);

end


function calculate_Callback(hObject, eventdata, handles)
    % Callback of the Go button
    % Just produce the output and close the figure
    %powerQuestionDlg_OutputFcn(hObject, eventdata, handles);
    uiresume(handles.figure1);
    %close;
end
