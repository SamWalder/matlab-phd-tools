function scopeScreenshot(instrumentID)
%SCOPESCREENSHOT Get screenshot from the occiliscope
%   
%   Sam Walder - University of Bristol - 2017
%   Inspired by the example code in the programming guide for the MSO6104A
%
% =========================================================================
%   Input arguments:
%       * instrumentID - Instrument object corresponding to the scope
%   Output Arguments
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

% Get the image data
rawData = query(obj1, ':DISPLAY:DATA? BMP, SCREEN, COLOR');

%byteData = myScope.ReadIEEEBlock(BinaryType_UI1)

% Output display data to a file:
strPath = "c:\scope\data\screen.bmp"


% Open file for output.
Open strPath For Binary Access Write Lock Write As #1
Put #1, , byteData ' Write data.
Close #1 ' Close file.
myScope.IO.Timeout = 5000