function [ figureHandle ] = scopePlot( wfmData, varargin )
%SCOPEPLOT Plots waveform data from an occiliscope
%
%   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * wfmData      - Wavform data file (can be the structure alredy
%                        loaded into the workspace, the filename, or the
%                        full directory and filename)
%       ** Specify the following using name-value pairs **
%       * channels     - Array of channels to plot (defaults to 1:16)
%       * figHandle    - If this is specified then the data will be added
%                        to the given figure
%       * origiAxe     - If specified as '1' the original axes scaling that
%                        was saved at wavform capture time will be used
%   Output arguments
%       * figureHandle - Handle to the created figure
%	Dependencies
%		* cmrmap
%		* tidyXaxis
% =========================================================================
%   Change Log:
%       * 2017 - Created
% =========================================================================

%% First deal with the inputs and set defaults

% Create an input parser object
p = inputParser;

% Define what the defaults should be
defaultChans = 1:16;
defaultOrigiAxe = 0;

% Set up the fileds of the input parser
addRequired(p,'wfmData');
addParameter(p,'channels', defaultChans);
addParameter(p,'figHandle', []);
addParameter(p,'origiAxe', defaultOrigiAxe);

% Parse the inputs
parse(p, wfmData, varargin{:});

% Get the matched inputs and put them where we were expecting them to be
channels = p.Results.channels;
origiAxe = p.Results.origiAxe;

% If the figure is empty make a new one
if isempty(p.Results.figHandle)
    figureHandle = figure;
else
    figureHandle = p.Results.figHandle;
end; % If


%% Make sure there is an axis on the figure and get its handle
if isempty(get(figureHandle, 'children'))
    % There is no axis on the figure, make one
    axesHandle = axes('parent', figureHandle);
else
    % We already have one and need to get its handle
    children = get(figureHandle, 'children');
    for i=1:length(children)
        if isa(children(i), 'matlab.graphics.axis.Axes')
            % We found one
            axesHandle = children(i);
        end; % If
    end; % For
end; % If


%% Figure out how the waveform data has been defined
% If it is a string...
if ischar(wfmData)
    %...then it could be in the current directory, or a fully qulified path
    % The load function will deal with this either way
    input = load([wfmData, '.mat']);
    % Get the name of the variable that was in the save file and move it to
    % a local variable we can work with. I don't like this as it has to use
    % eval :(
    name = fieldnames(input);
    wfmData = eval(['input.', name{1}]);
    
else
    % It is not a chacter array, so must be the actual structure.
    % We don't need to do anything in this case :)
    
end; % If        


%% Plot each of the waveforms
minDiv = 0;
maxDiv = 0;

for activeChan = 1:length(wfmData.channelData)
    % If this is a channel to plot AND it is not empty, then plot it
    if sum(ismember(activeChan, channels)) && ~isempty(wfmData.channelData(activeChan).yData)
        % All is well, do the plotting
        
        xData = wfmData.xData;
        yData = wfmData.channelData(activeChan).yData/wfmData.channelData(activeChan).scaling;
        
        % Set up the display name for the channel
        displayName = [wfmData.channelData(activeChan).name, ' (', num2str(wfmData.channelData(activeChan).scaling), ' ', wfmData.channelData(activeChan).units(1), '/div)'];
        
        % Finally do that whole plotting thing!!
        plot(axesHandle, xData, yData, 'displayName', displayName, 'color', cmrmap(length(wfmData.channelData), activeChan));
        
        % Store the min and max of the data
        minDiv = min([yData; minDiv]);
        maxDiv = max([yData; maxDiv]);
    
    end; % If
    figure(figureHandle);
    hold on;
end; % For

%% Change the X lims if they have asked for us to respect the original axis settings
if (origiAxe == 1) && isfield(wfmData, 'originalTimbase')
    timePerDiv = wfmData.originalTimbase.Range/10;
    switch wfmData.originalTimbase.Ref(1)
        case 'L'
            dataShift = 0.1;
            refOffset = 4;
        case 'C'
            dataShift = 0.5;
            refOffset = 0;
        case 'R'
            dataShift = 0.9;
            refOffset = -4;
    end; % Case
    
    windowCentre = wfmData.xData(end)*dataShift + refOffset*timePerDiv + wfmData.originalTimbase.Position;
    windowLeft = windowCentre - 5*timePerDiv;
    windowRight = windowCentre + 5*timePerDiv;
    
    set(axesHandle, 'XLim', [windowLeft, windowRight]);
    
end; % if
    

%% Do a little formatting of the figure
set(axesHandle, 'YLim', [floor(minDiv), ceil(maxDiv)]);
grid on;
legend('show');
formatPlot;
tidyAxes;
ylabel('Divisions');
set(gcf, 'color', [1, 1, 1]);


%% Fix the divisions so that they show as 1 tick per div
fixDivPerTic(axesHandle);


end