function [waveformData] = scopeGetWaves(scopeHandles, varargin)
%SCOPEGETWAVES Captures waveform data from scopes.
% Can handle multiple scopes, will also figure out and grab a scaling
% factor for each wave (i.e. the volts per division setting) and get a
% sutable x axis vector as well (So that you can plot with units of
% time)
%
%   Sam Walder - University of Bristol - 2013
%
% =========================================================================
%   Input arguments:
%       * scopeHandles - An array of handles to occiliscopes
%       * channelNames - (Optional) Array of names to give to channels
%       * getAll       - (Optional) Set this to '1' to get all of the 
%                        occiliscopes acquisition memory
%   Output Arguments
%       * waveformData - A structure with all of the waveform data
%	Dependencies
%		* timeStamp       - Sam Walder - 2016
%		* getWaveformData - Niall Oswald - September 2010
%		* getXScalingData - Niall Oswald - September 2010
% =========================================================================
%   Change Log:
%       * 2013 - Created
%       * 2017 - Sam Walder
%           General functionality and robustness improvments
%       * 2017 - Sam Walder
%           Added the 'getAll' functionality
% =========================================================================

%% Check how many inputs we received
switch nargin
    case 1
        % We need to set up some channel names
        channelNames = {'Ch1', 'Ch2', 'Ch3', 'Ch4', 'Ch5', 'Ch6', 'Ch7', 'Ch8'};
        getAll = 0;
    case 2
        % They have specified a second arguent, what is it?
        if isa(varargin{1}, 'cell')
            % They have passed channel names
            channelNames = varargin{1};
            getAll = 0;
        else
            % They have specified a value for getAll
            channelNames = {'Ch1', 'Ch2', 'Ch3', 'Ch4', 'Ch5', 'Ch6', 'Ch7', 'Ch8'};
            getAll = varargin{1};
        end; % if
    case 3
        % they gave both
        channelNames = varargin{1};
        getAll = varargin{2};
    otherwise
        % Not acceptable
        error('Invalid number of imputs to scopeGetWaves');
end; % Switch
        
%% Store a timestamp in the output
waveformData.timeStamp = timeStamp;

%% Get the data and put it into the output variable
% Set up the number of points variable to prevent stupidity occuring
nPoints = [];

for iScope = 1:length(scopeHandles)
    activeScope = scopeHandles(iScope);
    
    % Stop the scope, otherwies bad things can occour
    fprintf(activeScope, ':STOP');
    
    % Timebase modification
    % The scope will only return the points on the screen, so the following bit moves the timebase so that we get all of the points
    if getAll == 1
        % Get the current position
        setPos = str2double(query(activeScope, 'TIMebase:POSition?'));
        setRng = str2double(query(activeScope, 'TIMebase:RANGe?'));
        setRef = query(activeScope, ':TIMebase:REFerence?');

        % Figure out what the captured range is
        xInc = str2double(query(activeScope, ':WAVeform:XINCrement?'));     % Only works if the scope is stopped
        nPoints = str2double(query(activeScope, ':ACQuire:POINts?')); 
        captureLength = xInc * nPoints;

        % Set it so that all the data is visible
        %fprintf(activeScope, 'TIMebase:POSition 0'); % this actually shifts things off the screen
        % The following line works some of the time, but then if the delay
        % is to a precision too high for the longer ranges then the data
        % will be shifted over a bit and we miss some of the yData
        %fprintf(activeScope, ['TIMebase:RANGe', ' ', num2str(captureLength)]);
        % Instead, we can just set the range to a really high number and we
        % still get given all the data.
        fprintf(activeScope, ['TIMebase:RANGe', ' ', num2str(10)]);
        
        % Save the original settings in the output structure
        waveformData.originalTimbase(iScope).Position = setPos;
        waveformData.originalTimbase(iScope).Range = setRng;
        waveformData.originalTimbase(iScope).Ref = setRef;
    end; % if
    
    % Channel loop
    for iWaveform = 1:4
        activeChan = (iScope-1)*4 + iWaveform;
        
        % Check if the channel is actually switched on, if not skip it
        command = strcat(':CHANnel', num2str(iWaveform), ':DISPLAY?');
        ch_on = str2double(query(activeScope, command));
        
        if ch_on == 1,
            % Get the waveform data
            waveformData.channelData(activeChan).yData = getWaveformData(activeScope, iWaveform);
            
            % If we have not already done so we need to record the number
            % of points for later
            if isempty(nPoints)
                nPoints = length(waveformData.channelData(activeChan).yData);
            end; % If

            % Save the scaling data
            command = strcat(':CHANnel', num2str(iWaveform), ':SCALe?');
            waveformData.channelData(activeChan).scaling = str2double(query(activeScope, command));

            % Get the unit information
            command = strcat(':CHANnel', num2str(iWaveform), ':UNITS?');
            waveformData.channelData(activeChan).units = strcat(query(activeScope, command));

            % Give it a human name
            waveformData.channelData(activeChan).name = channelNames{activeChan};
        else
            disp(['Channel ', num2str(iWaveform), ' is not switched on']);
        end; % If
    end; % For
    
    % Put the timebase back if we have moved it
    if getAll == 1
        % Set the timebase back to what it was
        fprintf(activeScope, ['TIMebase:POSition', ' ', num2str(setPos)]);
        fprintf(activeScope, ['TIMebase:RANGe', ' ', num2str(setRng)]);
    end; % if
    
end; % For

%% Now get the x axis information
[~, ~, xIncrement] = getXScalingData(scopeHandles(1), 1);

% Assemble the vector
waveformData.xData = (0:xIncrement:(nPoints-1)*xIncrement)';

%% User Warning
% If more than one scope was used warn the user that it is assumed that the
% scopes are set to the same timebase
if length(scopeHandles) > 1
    disp('WARNING: It is assumed that the scopes are set to have the same timebase, you may want to fix this');
end; % If


end

