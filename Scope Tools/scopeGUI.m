function varargout = scopeGUI(varargin)
    %   DPTGUI Double Pulse Test Graphical User Interface
    %
    %   Sam Walder - University of Bristol - 2017 - sam.walder@bristol.ac.uk
    %
    % =====================================================================
    %   Input arguments
    %       * 
    %   Output arguments
    %       * 
    %	Dependencies
    %		* scopeConnect  - Sam Walder - 2017
    %		* scopeGetWaves - Sam Walder - 2013
    %
    %   Common function inputs
    %       hObject    - handle to figure
    %       eventdata  - reserved - to be defined in a future version of MATLAB
    %       handles    - structure with handles and user data (see GUIDATA)
    % =====================================================================
    %   Change Log:
    %       * 2017 - Created
    % =====================================================================

    % Begin initialization code - DO NOT EDIT... well, TBH, ignore that...
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @scopeGUI_OpeningFcn, ...
                       'gui_OutputFcn',  @scopeGUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
                   
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code

%=========================================================================%
%=========================================================================%
% Callbacks and integral functions
%=========================================================================%
%=========================================================================%

    %=========================================================================%
    % Opening Function
    %=========================================================================%
    function scopeGUI_OpeningFcn(hObject, eventdata, handles, varargin)
        %SCOPEGUI_OPENINGFCN Runs just before the figure becomes visible
        %
        % =====================================================================
        %   Input arguments
        %       * varargin  - Command line arguments to GUI
        % =====================================================================

        % Choose default command line output
        handles.output = hObject;
        
        % Set up some default values for stuff
        handles.selectedPowerTraces = [0, 0, 1, 1];
        handles.selectedSpectraTraces = [0, 0, 1, 0];
        handles.selectedBandPower = [0, 0, 0, 0];

        % Update handles structure
        guidata(hObject, handles);

        % Update the list of files in the popup
        updateFilelist(handles);


    %=========================================================================%
    % Output Function
    %=========================================================================%
    function varargout = scopeGUI_OutputFcn(hObject, eventdata, handles) 
        %SCOPEGUI_OUTPUTFCN Does something important... probably

        % Get default command line output from handles structure
        varargout{1} = handles.output;


    %=========================================================================%
    % Executes on button press in push_connect.
    %=========================================================================%
    function push_connect_Callback(hObject, eventdata, handles)
        %PUSH_CONNECT_CALLBACK

        % Connect an occiliscope
        handles.scope = scopeConnect;

        % Update handles structure
        guidata(hObject, handles);


    %=========================================================================%
    % Executes during object deletion, before destroying properties
    %=========================================================================%
    function scopeGUI_DeleteFcn(hObject, eventdata, handles)
        %SCOPEGUI_DELETEFCN Run at figure delection time

        % Disconnect the scope (if connected)
        if isfield(handles, 'scope')
            scopeDisconnect(handles.scope);
            delete(handles.scope)
        end; % If


    %=========================================================================%
    % Disconnect Callback
    %=========================================================================%
    % --- Executes on button press in push_disconnect.
    function push_disconnect_Callback(hObject, eventdata, handles)

        % Disconnect the scope (if connected)
        if isfield(handles, 'scope')
            scopeDisconnect(handles.scope);
            delete(handles.scope)
        end; % If

    %=========================================================================%
    % Edit PWD Callback
    %=========================================================================%
    function edit_pwd_Callback(hObject, eventdata, handles)

        % Update the list of files in the popup
        updateFilelist(handles);

        % Run update filename function
        updateFilename(handles);


    %=========================================================================%
    % Get Waveforms Callback
    %=========================================================================%
    % --- Executes on button press in push_get.
    function push_get_Callback(hObject, eventdata, handles)

        % Get the working directory
        workingDirectory = get(handles.edit_pwd, 'String');

        % Get the prefix
        prefix = get(handles.edit_prefix, 'String');

        % Figure out which file number we have gotten to
        numberFiles = nFiles(workingDirectory, prefix);

        % Get the file name
        filename = ['wfm_', prefix, '_', sprintf('%03.0f', numberFiles+1)];

        % Get the channel names
        channelNames{1} = get(handles.edit_ch1, 'String');
        channelNames{2} = get(handles.edit_ch2, 'String');
        channelNames{3} = get(handles.edit_ch3, 'String');
        channelNames{4} = get(handles.edit_ch4, 'String');
        
        % Find out if the user would like to get the whole aquasition
        % memory
        getAll = handles.checkbox6.Value;

        % Run the waveform capture tool
        waveformData = scopeGetWaves(handles.scope, channelNames, getAll);

        % Save the data
        save([workingDirectory, '\', filename], 'waveformData');
        assignin('base', filename, waveformData);

        % Run update filename function
        updateFilename(handles);

        % Update the list of files in the popup
        updateFilelist(handles);

    %=========================================================================%
    % Prefix Callback
    %=========================================================================%
    function edit_prefix_Callback(hObject, eventdata, handles)
        %EDIT_PREFIX_CALLBACK

        % Update the next name
        updateFilename(handles);    

    %=========================================================================%
    % Avalible Data Callback
    %=========================================================================%
    % --- Executes on selection change in popup_data.
    function popup_data_Callback(hObject, eventdata, handles)
        %POPUP_DATA_CALLBACK


    %=========================================================================%
    % Plot Callback
    %=========================================================================%
    % --- Executes on button press in push_plot.
    function push_plot_Callback(hObject, eventdata, handles)

        % Get the working directory
        workingDirectory = get(handles.edit_pwd, 'String');

        % Get the right filename
        selectedItem = get(handles.popup_data, 'Value');
        filenames = get(handles.popup_data, 'String');
        filename = filenames{selectedItem};

        % Load in the right file
        importedData = load(strcat(workingDirectory, '\wfm_', filename, '.mat'));
        
        % Find out if they want us to plot it onto the current figure
        if handles.checkbox4.Value;
            if isfield(handles, 'figHand');
                figHandle = handles.figHand;
            else
                figHandle = figure;
            end; % If
        else 
            figHandle = figure;
        end; % If
        
        % Figure out if they want us to use the original limits
        origiAxe = handles.checkbox7.Value;

        % Run the scopeplot function
        figHand = scopePlot( importedData.waveformData, 'origiAxe', origiAxe, 'figHandle', figHandle );
        handles.figHand = figHand;
        
        % Run the function to store the line handles for the scale stuff
        storeLineHandles(hObject, handles);
        
        % Set up the names in the scaling panel
        handles = guidata(hObject);     % Update to get the local copy of handles to be upto date with the external copy
        scaleUpdateChNames(handles);
        
        % Update the scale values in the scale edit boxes
        scaleUpdateEdit(handles);
        
        % Create a zoom object so that I can have a custom  zoom callback
        zoomObj = zoom;
        zoomObj.ActionPreCallback = @myprezoomcallback;
        zoomObj.ActionPostCallback = @mypostzoomcallback;
        zoomObj.Enable = 'off';
        
        % Set the zoom action to be horizontal
        setAxesZoomMotion(zoomObj, gca, 'horizontal');
        
    
    %=========================================================================%
    % Plot spectra callback
    %=========================================================================%
    function pushbutton19_Callback(hObject, eventdata, handles)
        % Get the waveform data
        importedData = loadWaveformData(handles);
        
        % Get the names of each of the channels
        names = getNames(importedData);
        
        % Ask the user which ones they want to plot
        handles.selectedSpectraTraces = powerQuestionDlg(names, handles.selectedSpectraTraces);
        traceNumbers = find(handles.selectedSpectraTraces);

        % Get the waveform data
        selectedYData = cell(1, sum(handles.selectedSpectraTraces));
        n = 1;
        for iCountToFour = 1:4
            if handles.selectedSpectraTraces(iCountToFour)
                selectedYData{n} = importedData.channelData(iCountToFour).yData;
                n = n + 1;
            end; % if
        end; % for

        % Run this through samfft
        frequency = cell(1, sum(handles.selectedSpectraTraces));
        amplitudes = cell(1, sum(handles.selectedSpectraTraces));
        for iCount = 1:sum(handles.selectedSpectraTraces)
            [frequency{iCount}, amplitudes{iCount}] = samfft(importedData.xData, selectedYData{iCount}, 'hann');
        end; % for

        % Create a new figure if they have not asked to reuse the current
        % axes
        if handles.checkbox5.Value
            figure(handles.specFig);
            hold on;
        else
            handles.specFig = figure();
        end; % if

        % Plot the requested things onto it
        for iCount = 1:sum(handles.selectedSpectraTraces)
            displayName = [importedData.channelData(traceNumbers(iCount)).name, ' (', importedData.channelData(traceNumbers(iCount)).units, 'S)'];
            loglog(frequency{iCount}, amplitudes{iCount}, 'displayName', displayName, 'color', cmrmap(sum(handles.selectedSpectraTraces), iCount));
            hold on;
        end; % for
        
        % Make it look a little more tidy
        legend('show');
        formatPlot;
        xlabel('Frequency (Hz)');
        tidyAxes;
        grid on;
        
        % Recolor the lines
        plotRecolor();
        
        % Update the handels structure
        guidata(hObject, handles);
        
    
    %=========================================================================%
    % Custom Function 1 Callback (add power trace)
    %=========================================================================%
    function push_c1_Callback(hObject, eventdata, handles)
        % Get the waveform data
        importedData = loadWaveformData(handles);
        
        % Get the names of each of the channels
        names = getNames(importedData);
    
        % We need to ask the user which traces it is that they want to
        % use
        handles.selectedPowerTraces = powerQuestionDlg(names, handles.selectedPowerTraces);
        % Error check
        if sum(handles.selectedPowerTraces) ~= 2
            % They are a fool, tell them about this
            error('Apparently you failed to select two traces');
        end; % if
        
        % Get the data for the two waveforms
        selectedYData = cell(1, 2);
        n = 1;
        for iCountToFour = 1:4
            if handles.selectedPowerTraces(iCountToFour)
                selectedYData{n} = importedData.channelData(iCountToFour).yData;
                n = n + 1;
            end; % if
        end; % for

        % Calculate the power waveform
        powerWave = selectedYData{1} .* selectedYData{2};
        
        % Move the current axis on the figure to the top
        %set(0, 'currentfigure', handles.figHand)
        figure(handles.figHand);
        topAxes = subplot(2, 1, 1, gca);
        
        % Add the additional axes to the figure
        bottomAxes = subplot(2, 1, 2);
        
        % Add the waveform to this
        plot(bottomAxes, importedData.xData, powerWave);
        
        % Copy the XLims from the top one to the bottom one
        subplot(2, 1, 1);
        XLim = get(gca, 'XLim');
        subplot(2, 1, 2);
        set(gca, 'XLim', XLim);
        
        % Format it a bit
        formatPlot;
        ylabel('Power (W)');
        xlabel('Time (s)')
        tidyAxes();
        grid;
        
        % Adjust the text sizing and color to improve the appearance a bit
        subplot(2, 1, 1);
        set(gca, 'FontSize', 14);
        set(legend, 'FontSize', 10);
        set(legend, 'Color', [1, 1, 1]);
        subplot(2, 1, 2);
        set(gca, 'FontSize', 14);
        set(legend, 'FontSize', 10);
        set(legend, 'Color', [1, 1, 1]);
        
        % Make the Y limit start from zero as this looks a bit better
        subplot(2, 1, 2);
        oldYLims = get(gca, 'ylim');
        set(gca, 'ylim', [0, oldYLims(2)]);
        
        % Tidy the y axis on the lower plot
        subplot(2, 1, 2);
        tidyAxes(gca, 'y');
        
        % Try and line up the y axes labels so that it looks nicer
        % We do this by inserting spaces to get the same length longest
        % tick label on each axis
        subplot(2, 1, 1);
        yTickLabelsTop = get(gca, 'yTickLabel');
        subplot(2, 1, 2);
        yTickLabelsBottom = get(gca, 'yTickLabel');
        
        longestTop = max(cellfun(@length, yTickLabelsTop));
        longestBottom = max(cellfun(@length, yTickLabelsBottom));
        
        spacesFactor = 2; % Number of spaces to add per character difference. This is just eyeballed
        
        if longestTop < longestBottom
            % Make the top longer
            labelToExtend = yTickLabelsTop{1};
            spacesToAdd = spacesFactor*(max(longestTop, longestBottom) - length(labelToExtend));
            extendedLabel = [repmat(' ', 1, spacesToAdd), labelToExtend];
            newTickLabels = [extendedLabel; yTickLabelsTop(2:end)];
            subplot(2, 1, 1);
            set(gca, 'yTickLabel', newTickLabels);
        elseif longestTop > longestBottom
            % Make the bottom longer
            labelToExtend = yTickLabelsBottom{1};
            spacesToAdd = spacesFactor*(max(longestTop, longestBottom) - length(labelToExtend));
            extendedLabel = [repmat(' ', 1, spacesToAdd), labelToExtend];
            newTickLabels = [extendedLabel; yTickLabelsBottom(2:end)];
            subplot(2, 1, 2);
            set(gca, 'yTickLabel', newTickLabels);
        else
            % Don't do anything
        end; % if
        
        
        % Make the top plot a little bigger
        subplot(2, 1, 1);
        set(gca, 'Position', [0.1304    0.5214    0.7746    0.4036]);
        
        % It is possible for the new tick labels to break the axis
        % alignment. Check what the position of each of them are and then
        % adjust
        topPosition = get(topAxes, 'position');
        bottomPosition = get(bottomAxes, 'position');
        
        if topPosition(1) ~= bottomPosition(1)
            newTopPositio = [bottomPosition(1), topPosition(2), bottomPosition(3), topPosition(4)];
            set(topAxes, 'position', newTopPositio);
        end; % if
        
        % Create a zoom object so that I can have a custom  zoom callback
        zoomObj = zoom;
        zoomObj.ActionPreCallback = @myprezoomcallback;
        zoomObj.ActionPostCallback = @mypostzoomcallback;
        zoomObj.Enable = 'off';
        
        % Set the zoom action to be horizontal
        setAxesZoomMotion(zoomObj, bottomAxes, 'horizontal');
        
        % Update the handels structure
        guidata(hObject, handles);
        
    %=========================================================================%
    % Custom Function 2 Callback (Calculate energy)
    %=========================================================================%
    % --- Executes on button press in push_c2.
    function push_c2_Callback(hObject, eventdata, handles)
        
        % Get the handle to the line
        % Start by setting the right figure to be current
        set(0, 'currentfigure', handles.figHand)
        subplot(2, 1, 2);
        line_handle = get(gca, 'Children');
        
        XLim = get(gca, 'XLim');
        
        % Setup the other inputs required
        start_threshold = str2double(handles.edit28.String);
        start_offset = XLim(1);
        stop_threshold = 0;
        floor = 0;
        
        % Integrate the curve
        [area, start, stop, range, peak] = integrateCurve(line_handle, start_threshold, start_offset, stop_threshold, floor);
        
        % Do something with the output of this?
        legend({strcat('Power (', num2eng(area), 'J)')});
        set(legend, 'FontSize', 10);
        
    %=========================================================================%
    % Custom Function 3 Callback (Calculate band power)
    %=========================================================================%
    function pushbutton20_Callback(hObject, eventdata, handles)
        % Calculate the band power for the psecified waveofrm within the
        % specified limits
        
        % Load the data
        importedData = loadWaveformData(handles);
        
        % Get the limits
        loLim = str2double(handles.bandLow.String);
        hiLim = str2double(handles.bandHigh.String);
        
        % Get the sampling frequency
        fs = 1 / (importedData.xData(2) - importedData.xData(1));
        
        % Get the names of each of the channels
        names = getNames(importedData);
        
        % Select a channel to use for the analysis
        handles.selectedBandPower = powerQuestionDlg(names, handles.selectedBandPower);
        if sum(handles.selectedBandPower) ~= 1
            % They are a fool, tell them about this
            error('Apparently you failed to select one trace');
        end; % if
        traceNumber = find(handles.selectedBandPower);
        
        % Get the right amount of the waveform data
        timePerDiv = importedData.originalTimbase.Range/10;
        switch importedData.originalTimbase.Ref(1)
            case 'L'
                dataShift = 0.1;
                refOffset = 4;
            case 'C'
                dataShift = 0.5;
                refOffset = 0;
            case 'R'
                dataShift = 0.9;
                refOffset = -4;
        end; % Case
        windowCentre = importedData.xData(end)*dataShift + refOffset*timePerDiv + importedData.originalTimbase.Position;
        windowLeft = windowCentre - 5*timePerDiv;
        windowRight = windowCentre + 5*timePerDiv;
        if windowLeft < 0
            windowLeft = 0;
        end; % if
        if windowRight > importedData.xData(end)
            windowRight = importedData.xData(end);
        end; % if
        ind = 1:length(importedData.xData);
        xStart = round(interp1(importedData.xData, ind, windowLeft));
        xEnd = round(interp1(importedData.xData, ind, windowRight));
        
        % Calculate the power
        p = bandpower(importedData.channelData(traceNumber).yData(xStart:xEnd), fs, [loLim, hiLim]);
        
        % Output the result
        disp(num2eng(p));
        
        baseVars = evalin('base', 'who');
        if find(cell2mat(strfind(baseVars, 'bandPowOut')))
            % Variable exists, get it
            outputVar = evalin('base', 'bandPowOut');
            outputVar = [outputVar, p];
            assignin('base', 'bandPowOut', outputVar);
        else
            % Does not exist, create it
            assignin('base', 'bandPowOut', p);
        end; % if
        
        % Update the handels structure
        guidata(hObject, handles);


    %=========================================================================%
    % Set current PWD Callback
    %=========================================================================%
    function pushbutton16_Callback(hObject, eventdata, handles)
        set(handles.edit_pwd, 'String', pwd);
        updateFilename(handles);
        updateFilelist(handles);
        
        
        
    %=========================================================================%
    % Smooth Plot Callback
    %=========================================================================%
    function pushbutton18_Callback(hObject, eventdata, handles)
        plotSmooth(handles.figHand);
        
     
%=========================================================================%
%=========================================================================%
% Rescale functions (bottom left panel)
%=========================================================================%
%=========================================================================%
    
    %=========================================================================%
    % Name to channel identification function
    %=========================================================================%
    function ChNo = getChannelNo(hObject)
    %GETCHANNELNO Figures out which of the channels an object belongs to
    %
    % =====================================================================
    %   Input arguments
    %       * hObject   - Handle of item to identify
    %   Output arguments
    %       * ChNo      - The number of the Ch that the object belongs to
    % =====================================================================
    
    % This works by just knowing all of the tags in advanced. Simple
    switch hObject.Tag
        case 'scaleChName1Text'
            ChNo = 1;
        case 'scaleDecreaseCh1'
            ChNo = 1;
        case 'scaleScaleEditCh1'
            ChNo = 1;
        case 'scaleIncreaseCh1'
            ChNo = 1;
        case 'scaleChName2Text'
            ChNo = 2;
        case 'scaleDecreaseCh2'
            ChNo = 2;
        case 'scaleScaleEditCh2'
            ChNo = 2;
        case 'scaleIncreaseCh2'
            ChNo = 2;
        case 'scaleChName3Text'
            ChNo = 3;
        case 'scaleDecreaseCh3'
            ChNo = 3;
        case 'scaleScaleEditCh3'
            ChNo = 3;
        case 'scaleIncreaseCh3'
            ChNo = 3;
        case 'scaleChName4Text'
            ChNo = 4;
        case 'scaleDecreaseCh4'
            ChNo = 4;
        case 'scaleScaleEditCh4'
            ChNo = 4;
        case 'scaleIncreaseCh4'
            ChNo = 4;
        otherwise
            error('The handle that was passes cannot be identified by this function');
    end
    
    
    %=========================================================================%
    % Scale increase buttons callback
    %=========================================================================%
    function scaleIncrease_Callback(hObject, eventdata, handles)
        % This function is run when any of the increase buttons are
        % pressed
        
        % Figure out which line line handle this is for
        lineNumber = getChannelNo(hObject);
        lineHandle = handles.scale.lineHandles(lineNumber);
        
        % Get the current line name
        lineName = get(lineHandle, 'DisplayName');

        % Find the locations of the opening bracket and the space after
        openIdx = strfind(lineName, '(');
        closeIdx = openIdx + strfind(lineName(openIdx:end), ' ');

        % Convert the characters into a number
        oldScale = str2double(lineName(openIdx+1:closeIdx-1));
        
        % Decide on what the new scale should be
        newScale = oldScale / 2;
        
        % Run the rescaling function
        scaleChangeScale(lineHandle, newScale);
        
        % Update the edit box text
        scaleUpdateEdit(handles);

    %=========================================================================%
    % Scale decrease buttons callback
    %=========================================================================%
    function scaleDecrease_Callback(hObject, eventdata, handles)
        % This function is run when any of the decrease buttons are
        % pressed
        
        % Figure out which line line handle this is for
        lineNumber = getChannelNo(hObject);
        lineHandle = handles.scale.lineHandles(lineNumber);
        
        % Get the current line name
        lineName = get(lineHandle, 'DisplayName');

        % Find the locations of the opening bracket and the space after
        openIdx = strfind(lineName, '(');
        closeIdx = openIdx + strfind(lineName(openIdx:end), ' ');

        % Convert the characters into a number
        oldScale = str2double(lineName(openIdx+1:closeIdx-1));
        
        % Decide on what the new scale should be
        newScale = oldScale * 2;
        
        % Run the rescaling function
        scaleChangeScale(lineHandle, newScale);
        
        % Update the edit box text
        scaleUpdateEdit(handles);


    %=========================================================================%
    % Scale manual adjust callback
    %=========================================================================%
    function ScaleEdit_Callback(hObject, eventdata, handles)
        % This will cause the line to be scalled to the new setting
        
        % First establish which line this is for
        lineNumber = getChannelNo(hObject);
        lineHandle = handles.scale.lineHandles(lineNumber);
        
        % Establish what the new scale is to be
        newScale = str2double(hObject.String);
        
        % Run the rescaling function on the given line
        scaleChangeScale(lineHandle, newScale);
        
    %=========================================================================%
    % Populate channel names function
    %=========================================================================%
    function scaleUpdateChNames(handles)
        % This will populate the static text next to each of the sets of
        % controls with the channel names
        
        for chCount = 1:4
            % Check that the line exists for this channel
            if length(handles.scale.lineHandles) >= chCount
                % Get the channel name
                name = get(handles.scale.lineHandles(chCount), 'displayName');

                % Get the bit without the units/div bit
                endIdx = strfind(name, '(');
                shortName = name(1:endIdx-1);

                % Set the text name to this
                eval(strcat('set(handles.scaleChName', num2str(chCount), 'Text, ''String'', shortName)'));
            else
                % The channel was not on on the scope, so just fill with
                % some rubbish text
                eval(strcat('set(handles.scaleChName', num2str(chCount), 'Text, ''String'', ''-'')'));
            end; % if
        end; % for
        
    
    %=========================================================================%
    % Update edit box scalling numbers
    %=========================================================================%
    function scaleUpdateEdit(handles)
        % This will populate the edit text for each of the channels with
        % the scalling number
        
        for chCount = 1:4
            % Check that the line exists for this channel
            if length(handles.scale.lineHandles) >= chCount
                % Get the channel name
                name = get(handles.scale.lineHandles(chCount), 'displayName');

                % Find the locations of the opening bracket and the space after
                % this
                openIdx = strfind(name, '(');
                closeIdx = openIdx + strfind(name(openIdx:end), ' ');

                % Convert the characters into a number
                scale = name(openIdx+1:closeIdx-1);

                % Set the edit text to this
                eval(strcat('set(handles.scaleScaleEditCh', num2str(chCount), ', ''String'', scale)'));
            else
                % The channel was not on on the scope, so just fill with
                % some rubbish text
                eval(strcat('set(handles.scaleScaleEditCh', num2str(chCount), ', ''String'', ''-'')'));
            end; % if
        end; % for
    
    %=========================================================================%
    % Change the scale of a channel
    %=========================================================================%
    function scaleChangeScale(lineHandle, newScale)
        % This will look up the current scale of the given line and change
        % it to the given scale
        
        % Get the current line name
        lineName = get(lineHandle, 'DisplayName');

        % Find the locations of the opening bracket and the space after
        % this
        openIdx = strfind(lineName, '(');
        closeIdx = openIdx + strfind(lineName(openIdx:end), ' ');

        % Convert the characters into a number
        oldScale = str2double(lineName(openIdx+1:closeIdx-1));
        
        % Change the yData to the new scale
        oldYData = get(lineHandle, 'yData');
        newYData = oldYData .* (oldScale/newScale);
        set(lineHandle, 'yData', newYData);
        
        % Change the lable to show the new scale
        newDisplayName = [lineName(1:openIdx), num2str(newScale), lineName(closeIdx-1:end)];
        set(lineHandle, 'DisplayName', newDisplayName);
    
    %=========================================================================%
    % Get the line handles into the main handel structure function
    %=========================================================================%
    function storeLineHandles(hObject, handles)
        % This will get the handles of the lines in the figure and add them
        % to the main handle structure
        
        % First we need to get the right axes
        figAxes = findobj(handles.figHand, 'type', 'axes');
        topAxes = figAxes(1);   % Just select the first one
        
        % Now get the lines
        lineHandles = findobj(topAxes, 'type', 'line');
        
        % Store these
        handles.scale.lineHandles = lineHandles;
        
        % Update the handels structure
        guidata(hObject, handles);
        
        
%=========================================================================%
%=========================================================================%
% Custom Functions
%=========================================================================%
%=========================================================================%

    %=========================================================================%
    % Number of files
    %=========================================================================%
    function count = nFiles(cwd, prefix)
        % Gets the number of waveform files that are already saved in the
        % current directory with the given prefix
        % =====================================================================
        %   Input arguments
        %       * cwd    - The present working directory
        %       * prefix - The prefix to check for
        % =====================================================================

        % Fist get all waveform files in the current directory
        wfmFiles = listWfmFiles(cwd);

        % Filter these to just those with the given prefix
        matchedFiles = cell(length(wfmFiles), 1);
        x = 1;
        for i=1:length(wfmFiles)
            stamp = wfmFiles{i}(1:end-4);
            if strcmp(prefix, stamp)
                % We have a hit!
                matchedFiles{x} = wfmFiles{i}(end-2:end);
                x = x+1;
            end; % if
        end; % for
        matchedFiles = matchedFiles(1:x-1);

        % Find the largest number associated with one of these
        max = 0;
        for i=1:length(matchedFiles)
            number = str2double(matchedFiles{i});
            if number > max
                max = number;
            end; % If
        end; % For

        count = max;


    %=========================================================================%
    % List of files
    %=========================================================================%
    function list = listWfmFiles(cwd)
        % Gets a list of all the waveform files in a directory
        % =====================================================================
        %   Input arguments
        %       * cwd    - The present working directory
        % =====================================================================

        % Fist get all the objects in the current directory
        allObjects = dir(cwd);

        % Now, figure out which of these are waveform saves
        wfmFiles = cell(length(allObjects), 1);
        x = 1;
        for i=1:length(allObjects)
            if length(allObjects(i).name)>6
                beggining = allObjects(i).name(1:4);
                if strcmp('wfm_', beggining)
                    % We have a hit!
                    wfmFiles{x} = allObjects(i).name(5:end-4);
                    x = x+1;
                end; % if
            end; % if
        end; % for
        list = wfmFiles(1:x-1);

    %=========================================================================%
    % Update next file name function
    %=========================================================================%
    function updateFilename(handles)
        % UPDATEFILENAME updates the displayed next file name

        % Get the working directory
        workingDirectory = get(handles.edit_pwd, 'String');

        % Get the prefix
        prefix = get(handles.edit_prefix, 'String');

        % Get the number of files
        numberFiles = nFiles(workingDirectory, prefix);

        % Set the next filename
        nextFilename = ['wfm_', prefix, '_', sprintf('%03.0f', numberFiles+1)];
        set(handles.text_nextName, 'String', ['Next Name: ', nextFilename]);


    %=========================================================================%
    % Update files list function
    %=========================================================================%
    function updateFilelist(handles)
        % UPDATEFILELIST updates the list of files that can be plotted

        % Get the working directory
        workingDirectory = get(handles.edit_pwd, 'String');

        % Get the list of files
        list = listWfmFiles(workingDirectory);

        % Check for an empty list
        if isempty(list)
            list = '-No files found-';
        end; % if

        % Put this in the listbox
        set(handles.popup_data, 'String', list);
        set(handles.popup_data, 'Value', 1);
        
  
        
    %=========================================================================%
    % Load the waveform data
    %=========================================================================%       
    function importedData = loadWaveformData(handles)
    % Gets the waveform data given the selections in the figure
    
        % Get the working directory
        workingDirectory = get(handles.edit_pwd, 'String');

        % Get the right filename
        selectedItem = get(handles.popup_data, 'Value');
        filenames = get(handles.popup_data, 'String');
        filename = filenames{selectedItem};

        % Load in the right file
        importedData = load(strcat(workingDirectory, '\wfm_', filename, '.mat'));
        importedData = importedData.waveformData;

        
    %=========================================================================%
    % Get channel names
    %=========================================================================%       
    function names = getNames(importedData)
        
        names = cell(1, 4);
        for iChan = 1:length(importedData.channelData)
            raw = importedData.channelData(iChan).name;
            if ~strcmp(raw, '')
                names{iChan} = raw;
            else
                names{iChan} = '- Not in use -';
            end; % if
        end; % for
        
    %=========================================================================%
    % Callback that runs before a zoom event
    %=========================================================================%
    function myprezoomcallback(obj, evd)
        % Do nothing
        
    %=========================================================================%
    % Callback that runs after a zoom event
    %=========================================================================%
    function mypostzoomcallback(obj, evd)
        % First find all the axis objects
        axis = findobj(gcf, 'type', 'axes');

        % Get the xlims that have been set on the current axes
        newXLim = get(gca, 'xlim');

        % Apply this to all the other axis
        for i=1:length(axis)
            set(axis(i), 'xlim', newXLim);
        end; % for

        % Rejig the xAxes on the last axis
        tidyAxes(axis(1));

        % Get the ticks from the last axis and apply them to the other ones
        newXTick = get(axis(1), 'xtick');
        for i=1:length(axis)
            set(axis(i), 'xtick', newXTick);
        end; % for
