function [waveformData] = getWaveformData(instrumentID, channelID)
%GETWAVEFORMDATA Gets waveform data from an Agilent occiliscope
%   Agilent data acquisition code. This function returns the captured 
%   waveform data. Assumes 'DIG' has been issued (since this function needs
%   to be called for each channel).
%
%   Niall Oswald - September 2010 - no4208@bris.ac.uk
%   Sam Walder - University of Bristol - 2013
%   Apollo Charalambous - University of Bristol - 2013
%
% =========================================================================
%   Input arguments:
%       * instrumentID - Instrument object corresponding to the scope
%       * channelID    - Numerical value (1-4) which specifies the channel 
%                        to query
%   Output Arguments
%       * waveformData - A structure with all of the waveform data
% =========================================================================
%   Change Log:
%       * 2010 - Niall Oswald - Created
%       * 2011/10 - Niall Oswald
%           Modified to single precision
%       * 2013 - Sam Walder
%           General functionality and robustness improvments
%       * 2013 - Apollo Charalambous
%           See inline notes
%       * 2016 - Sam Walder
%           Added feture to check that the scope is stopped
%       * 2017 - Sam Walder
%           Added a line that ensures the scope sends all the data it can
% =========================================================================

    % Ensure the scope is stopped
    fprintf(instrumentID, ':STOP');                                             % Stop the scope so that the RAW data becomes avalible
    
    fprintf(instrumentID, ':WAVeform:SOUR CHAN%d',channelID);                   % Set waveform source to appropriate channel
    fprintf(instrumentID, ':WAVeform:POINts:MODE RAW');                         % Se the mode so that we get the RAW data
    fprintf(instrumentID, ':WAVeform:POINts MAX');                              % Set the number of points to maximum
                                                                                
                                                                                % Important: To use the raw acquisition record the oscilloscope must be STOPPED.                                                                                   
    fprintf(instrumentID, ':WAV:DATA?');                                        % Request waveform data
    fscanf(instrumentID, '%c', 1);                                              % Read first byte of header (#)
    headerByte2 = fscanf(instrumentID, '%c', 1);                                % Read second header byte (number of header bytes to follow)        
    waveformLength = fscanf(instrumentID, '%c', str2double(headerByte2));       % Read remainder of header (number of data bytes to follow)
    waveformData = fread(instrumentID, str2double(waveformLength), 'uint8');    % Read waveform data
    fscanf(instrumentID, '%s');                                                 % Read trailing newline characters to clear buffers
    % Convert to volts, adjust for offsets
    [yReference, yOrigin, yIncrement] = getYScalingData(instrumentID,channelID);
    waveformData=single(((waveformData-yReference)*yIncrement)+yOrigin);
    
    
    