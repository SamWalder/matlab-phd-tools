function [instrumentHandle] = scopeConnect()
%   SCOPECONNECT Connects to a supported occiliscope
%
%   Niall Oswald September 2010 - no4208@bris.ac.uk
%   Originally called "connectInstrument.m"
%   Sam Walder - University of Bristol - 2016 - sam.walder@bristol.ac.uk
%
% =========================================================================
%   Input arguments
%       * 
%   Output arguments
%       * instrumentHandle - Handle of the connected object
%	Dependencies
%		* findInstrument - https://uk.mathworks.com/matlabcentral/fileexchange/25593-findinstrument-find-and-connect-to-instruments-using-the-model-or-manufacturer-name
% =========================================================================
%   Change Log:
%       * 2010 - Created by Niall Oswald
%       * 2017 - Modified by Sam Walder
%           Added functionality for automatic detection of connected
%           objects
% =========================================================================

% Define supported occiliscopes
supportedModels = {'MSO6104A', 'DSO7104A'};

% Look for a one of the supported scopes
instrumentHandle = [];
i = 1;
while isempty(instrumentHandle) && i<=length(supportedModels)
    connectedModel = supportedModels{i};
    instrumentHandle = findInstrument(connectedModel);
    i = i + 1;
end; % Else

% Check to see that this has worked
if ~isempty(instrumentHandle)
    % We were sucessfull
else
    % We were not :(
    error('Could not find a supported occiliscope. If one is connected, are you sure it is on the list?');
end; % If

% Set up occiliscope parameters
switch connectedModel
    case 'MSO6104A'
        scopeEOSmode = 'read&write';
        scopeInputBufferSize = 8200000;
        scopeOutputBufferSize = 8200000;
    case 'DSO7104A'
        scopeEOSmode = 'read&write';
        scopeInputBufferSize = 8200000;
        scopeOutputBufferSize = 8200000;
    otherwise
        error('Settings have not yet been provided for this occiliscope');
end; % Switch

set(instrumentHandle, 'InputBufferSize', scopeInputBufferSize);
set(instrumentHandle, 'OutputBufferSize', scopeOutputBufferSize);
set(instrumentHandle, 'EOSMode', scopeEOSmode);

% Connect to instrument object
fopen(instrumentHandle);

% Tell the user it worked
msgbox([connectedModel, 'connected!'],'Success');